<?php
/**
 * A handler to display the description with html
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_description extends views_handler_field_serialized {
  function render($values) {
    $value = $values->{$this->field_alias};

    if ($this->options['format'] == 'unserialized') {
      return check_plain(print_r(unserialize($value), TRUE));
    }
    elseif ($this->options['format'] == 'key' && !empty($this->options['key'])) {
      $value = (array) unserialize($value);
      //var_dump($value[$this->options['key']]);
      return $value[$this->options['key']];
    }

    return $value;
  }
}
