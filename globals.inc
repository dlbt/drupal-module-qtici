<?php
/**
 * @file - Globals
 */

/**
 * Media extensions to be stored in the DB
 */
global $_qtici_extensions;
$_qtici_extensions = array(
  'mp4',
  'mp3',
  'ogg',
  'webm',
  'flv',
);

/**
 * Question types defined in the xml
 */
global $_qtici_questionTypes;
$_qtici_questionTypes = array(
  0 => 'SCQ', // Single Choice
  1 => 'MCQ', // Multiple Choice
  2 => 'FIB', // Fill In Blanks
  3 => 'KPRIM', // Faulttolerance
);

/**
 * Topics
 */
global $_qtici_topics;
$_qtici_topics = array(
  'herhaling' => 'Herhaling',
  'verstavaardigheid' => 'Verstavaardigheid',
  'variatie' => 'Variatie',
);

/**
 * Levels
 */
global $_qtici_levels;
$_qtici_levels = array(
  'A' => t('A1/2'),
  'B' => t('B1/2'),
  'C' => t('C1/2'),
);

/**
 * Exercises types
 */
global $_qtici_exercisesTypes;
$_qtici_exercisesTypes = array(
  'SCQ' => t('SCQ'),
  'MCQ' => t('MCQ'),
  'FIB' => t('FIB'),
  'KPRIM' => t('KPRIM'),
  'DAD' => t('DAD'),
  'MRK' => t('MRK'),
  'VID' => t('VID'),
  'REC' => t('REC'),
);

/**
 * Exercises classes
 */
global $_qtici_exercisesClasses;
$_qtici_exercisesClasses = array(
  'SCQ' => 'SingleChoiceQuestion',
  'MCQ' => 'MultipleChoiceQuestion',
  'FIB' => 'FillInQuestion',
  'KPRIM' => 'KPRIMQuestion',
  'DAD' => 'DragAndDropQuestion',
  'MRK' => 'MarkerQuestion',
  'VID' => 'VideoQuestion',
  'REC' => 'RecorderQuestion',
);

/**
 * Taxonomy configuration (temporal)
 */
global $_qtici_vid;
global $_qtici_field_name;
$_qtici_vid = 1;
$_qtici_field_name = 'field_tags';
