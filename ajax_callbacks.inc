<?php

/**
 * @file - AJAX callbacks
 */

/*
 * Ajax function
 * If a teacher clicks on the (Un)publish or Delete button in get_overview(), Ajax will handle this request.
 */

function process_overview_test() {
  //object from the class test for accessing his functions
  $testObj = new Test();
  $test_info = $_POST['var'];
  $checkArray = $test_info[0];
  $action = $test_info[1];

  if ($action == '(un)publish') {

    foreach ($checkArray as $testid) {
      _qtici_publishTest($testid);
    }

    drupal_set_message(t('De records werden succesvol (niet) zichtbaar gemaakt.'));
  }
  elseif ($action == 'delete') {

    foreach ($checkArray as $testid) {
      $testObj->deleteTestByTestIDOROlatID($testid);
    }
    drupal_set_message(t('De records werden succesvol verwijderd.'));
  }
  exit;
}

function _qtici_simple_ajax($form, &$form_state) {
  return $form['content'];
}

function _qtici_statsFilter_ajax($form, &$form_state) {
  unset($form_state['values']['testid']);
  return $form['list'];
}

/**
 * the callback funtion for the auto complete on the exercie overview page
 */
function _qtici_autocomplete($string) {
  $matches = array();

  // Some fantasy DB table which holds cities
  $query = db_select('qtici_test', 't');

  // Select rows that match the string
  $return = $query
      ->fields('t', array('title'))
      ->condition('t.title', '%' . db_like($string) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();

  // add matches to $matches
  foreach ($return as $row) {
    $matches[$row->title] = check_plain($row->title);
  }

  // return for JS
  drupal_json_output($matches);
}

/*
 * callback funtion for refreshing the table
 */

function overview_table_callback($form, &$form_state) {
  return $form['overview_table'];
}

function check_answers_ajax_callback($form, $form_state) {

  //get the item id out of the triggering element
  if (($tmp = strrchr($form_state['triggering_element']['#name'], '_')) !== false) {
    $itemid = substr($tmp, 1);
  }

  //higher the attempts done
  $_SESSION['exercise']['attempts']['item_' . $itemid] += 1;

  if (isset($form['items'])) {
    return $form['items'][$itemid]["content_" . $itemid];
  }
  else {
    return $form["content_" . $itemid];
  }
}

function show_answers_ajax_callback($form, $form_state) {

  //get the item id out of the triggering element
  if (($tmp = strrchr($form_state['triggering_element']['#name'], '_')) != false) {
    $itemid = substr($tmp, 1);
  }

  //get the maximum attempts that are allowed
  $max_attempts = $form_state['values']['attempts_' . $itemid];

  if (isset($form['items'])) {
    return $form['items'][$itemid]["content_" . $itemid];
  }
  else {
    return $form["content_" . $itemid];
  }
}
