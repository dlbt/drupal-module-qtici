<?php

/**
 * @file - All forms go in this file
 */
/*
 * In this function the sessions are cleared. The sessions are created and used in qtici_pagePreview() & qtici_pagePreview_submit().
 * The reason why they are cleared here is because Drupal executes Callback function twice (no idea why).
 */

function qtici_pageOverview_form($form, &$form_state) {
  if (isset($_SESSION['form_state'])) {
    unset($_SESSION['form_state']);
  }
  if (isset($_SESSION['testid'])) {
    unset($_SESSION['testid']);
  }
  if (isset($_SESSION['totalScore'])) {
    unset($_SESSION['totalScore']);
  }

  //initialize the variables needed
  $form = array();
  drupal_add_css(drupal_get_path('module', 'qtici') . '/css/css.css');
  $default = 0;
  $title = '';

  //look if a value is selected for the row count and if not give it a default
  if (empty($form_state['values']['entries'])) {
    $default = 10;
  }
  else {
    $default = $form_state['values']['entries'];
  }

  //look if a value is given to te title that you want to search for and if not make it blank
  if (empty($form_state['values']['search'])) {
    $title = '';
  }
  else {
    $title = $form_state['values']['search'];
  }

  //make a dropdown form for choosing the row count
  $form['entries'] = array(
    '#name' => 'entries',
    '#type' => 'select',
    '#title' => t('Aantal rijen per pagina: '),
    '#default_value' => $default,
    '#options' => array(10 => 10, 25 => 25, 50 => 50, 100 => 100),
    '#description' => t(''),
    '#ajax' => array(
      'callback' => 'overview_table_callback',
      'wrapper' => 'table_excercises',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  //Make a search form for searching for titles
  $form['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Zoeken:'),
    '#description' => t('Druk Enter om te zoeken'),
    '#size' => 35,
    '#prefix' => '<div class="dataTables_filterDrupal">',
    '#suffix' => '</div>',
    '#autocomplete_path' => 'oefeningen/overview/autocomplete',
    '#ajax' => array(
      'callback' => 'overview_table_callback',
      'wrapper' => 'table_excercises',
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array(),
    ),
  );

  //look if the user is in the overlay
  if (module_exists('overlay') && path_is_admin($_GET['q'])) {
    //make an url for ajax call so that it also works the second time
    $_GET['q'] = "admin/test/manage";
  }
  else {
    //make an url for ajax call so that it also works the second time
    $_GET['q'] = "oefeningen/overview";
  }

  if (module_exists('mobile_theme')) {
    //get the table and the pager you want to show
    $array = get_exercise_overview_mobile($default, $title);
  }
  else {
    //get the table and the pager you want to show
    $array = get_exercise_overview($default, $title);
  }

  //look if the logged in user is a teacher
  if (user_access('teacher')) {
    //make the table
    $form['overview_table'] = array(
      '#type' => 'tableselect',
      '#header' => $array['header'],
      '#options' => $array['options'],
      '#empty' => t('No content available.'),
      '#prefix' => '<div id="table_excercises">',
      '#suffix' => $array['html'] . '</div>',
    );
  }
  else {
    $form['overview_table'] = array(
      '#prefix' => '<div id="table_excercises">',
      '#suffix' => $array['html'] . '</div>',
      '#theme' => 'table',
      '#header' => $array['header'],
      '#rows' => $array['options'],
      '#empty' => t('No content available.'),
    );
  }

  //look if the logged in user is a teacher
  if (user_access('teacher')) {
    //delete button
    $form['overview_table_delete'] = array(
      '#type' => 'button',
      '#value' => t('Delete'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('_qtici_deleteTest_submit'),
      '#attributes' => array('onclick' => 'if(!confirm("' . t("Wilt u de test echt verwijderen?") . '")){return false;}'),
    );

    //publish unpublish button
    $form['overview_table_publish'] = array(
      '#type' => 'button',
      '#value' => t('(Un)publish'),
      '#executes_submit_callback' => TRUE,
      '#submit' => array('_qtici_publishTest_submit'),
    );
  }

  return $form;
}

function qtici_item_entity_form($form, &$form_state, $entity) {
  //get the possibilities for the entity
  $possibilities = _qtici_loadPossibilitiesByItemID($entity->id);

  //get the settings of the test
  $testoptions = qtici_test_entity_load($_SESSION['entity_test']['testid']);

  //insert the possibilities and answers for that item in the options array
  $options = array();
  $answers = null;
  $possibilitiesCount = 0;

  //make the options and answers
  foreach ($possibilities as $posibility) {

    //higher the possibilities count
    $possibilitiesCount++;

    //process the blob value
    $answerArray = unserialize($posibility->getAnswer());

    //fill in the answer
    if ($posibility->is_correct == 1) {
      $answers .= $answerArray["value"] . " ";
    }

    //insert al the possible options
    $options += array($posibility->id => $answerArray["value"]);
  }

  if (empty($_SESSION['exercise']['show_answer_' . $entity->id])) {
    $_SESSION['exercise']['show_answer_' . $entity->id] = 0;
  }
  
  if (empty($_SESSION['exercise']['check_answer_' . $entity->id])) {
    $_SESSION['exercise']['check_answer_' . $entity->id] = 0;
  }

  if (empty($_SESSION['exercise']['attempts']['item_' . $entity->id])) {
    $_SESSION['exercise']['attempts']['item_' . $entity->id] = 1;
  }

  //get the answers of the textboxes
  $answerTextboxesText = NULL;
  foreach ($possibilities as $answer) {
    if (!empty($answer->answer)) {

      //process the blob value
      $answerArray = unserialize($answer->answer);
      if ($entity->type == "FIB") {
        if (is_array($answerArray["value"])) {
      	  $answerArray = $answerArray["value"];
        }
      }
      $answerTextboxesText .= $answerArray['value'] . "/";
    }
  }

  //check if the logged in user has the good rights
  global $user;
  $check = array_intersect(array('teacher', 'administrator'), array_values($user->roles));
  if (empty($check) ? FALSE : TRUE) {
    //give the div for the contextual menu
    $form["contextual_menu_begin_" . $entity->id] = array(
      '#markup' => "<div class=\"contextual-links-region\">" . theme('contextual', array('destination' => 'item/' . $entity->id,
        'links' => array(array('admin/item/' . $entity->id . '/edit', t('Oefening bewerken')))))
    );
  }

  //attach js to form
  $form['#attached']['library'] = array(
    drupal_add_library('system', 'ui.draggable'),
    drupal_add_library('system', 'ui.droppable'),
    drupal_add_library('system', 'ui.selectable'),
    drupal_add_library('system', 'ui.position'),
  );

  //get the form for the item
  $info = array();

  $form += $entity->makeExerciseForm($info);
  foreach ($info as $fid => $values) {
    $audio = FALSE;
    if ($values['type'] == 'audio') {
      $audio = TRUE;
    }
    $file = file_load($fid);
    $url = file_create_url($file->uri);
    _qtici_addFlowPlayer($values['class'] . $fid, $url, $audio);
  }

  //initialize a variable that holds the possibility ids with a komma in between for putting it in a hidden
  //textbox so you know which textboxes and how manny you need to check later
  $textboxen = "";
  foreach ($possibilities as $key) {
    $textboxen .= $key->id . '/';
  }

  //holds the textboxes that were generated, soo you can call them in chack answers
  $form['texboxenFIBDAD_' . $entity->id] = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $textboxen,
    '#required' => FALSE,
  );

  //add css for displaying the textboxes next to each other
  drupal_add_css('.options > div { border: solid black 1px; padding: 3px;
  margin: 5px; color: black; background-color: #61BCED; display: inline-block; z-index: 5} .clear { clear: left; } .item { margin-bottom: 20px; }', $option['type'] = 'inline');
  drupal_add_css('div.container-inline { display: inline; }', $option['type'] = 'inline');
  drupal_add_css('div.container-inline .form-item { display: inline; }', $option['type'] = 'inline');

  //the answer in a hidden field for getting it in the callback of show_answers
  $form['answer_' . $entity->id] = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $answers,
    '#required' => FALSE,
  );

  //set the count of the possibilities
  $form['possibilitiesCount_' . $entity->id] = array(
    '#type' => 'hidden',
    '#attributes' => array('id' => "possibilitiesCount_" . $entity->id),
    '#title' => '',
    '#default_value' => $possibilitiesCount - 1,
    '#required' => FALSE,
  );

  //the type of the item
  $form['type_' . $entity->id] = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $entity->type,
    '#required' => FALSE,
  );

  //the max attempts of the item
  $form['attempts_' . $entity->id] = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $entity->max_attempts,
    '#required' => FALSE,
  );

  //the quotation of the item
  $form['quotation_' . $entity->id] = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $entity->quotation,
    '#required' => FALSE,
  );

  //the score of the item
  $form['score_' . $entity->id] = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $entity->score,
    '#required' => FALSE,
  );

  //the textbox answers in a hidden field for getting it in the callback of show_answers
  $form['answerTextboxes_' . $entity->id] = array(
    '#type' => 'hidden',
    '#title' => '',
    '#default_value' => $answerTextboxesText,
    '#required' => FALSE,
  );

  //make a container for the answer label
  $form["content_" . $entity->id] = array(
    '#type' => 'container',
    '#prefix' => '<div id="box_' . $entity->id . '">',
    '#suffix' => '</div>',
  );

  $form["content_" . $entity->id]['labelAnswers_' . $entity->id] = array();

  //change the visibility of the answer variable
   $visibility = NULl;
  if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'show_answers_' . $entity->id) {
   
    if (isset($_SESSION['qtici']['visibility_' . $entity->id])) {
      $visibility = $_SESSION['qtici']['visibility_' . $entity->id];
    }

    if ($visibility === 0) {
      $visibility = 1;
    }
    else {
      $visibility = 0;
    }
  }
  
  //look if the button shculd be displayed
  if ($_SESSION['exercise']['check_answer_' . $entity->id] != 5 && ($testoptions->check_answer == NULL || $testoptions->check_answer == 1)) {
    //check_answer button
    $form["content_" . $entity->id]['check_answer_' . $entity->id] = array(
      '#type' => 'button',
      '#name' => 'check_answer_' . $entity->id,
      '#ajax' => array(
        'callback' => 'check_answers_ajax_callback',
        'wrapper' => 'box_' . $entity->id,
      ),
      '#value' => t('Controleer mijn antwoord.'),
    );
  }

  if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'check_answer_' . $entity->id) {
    //$item = qtici_itemType_entity_load($entity->id);
    $result = $entity->checkAnswer($form_state);
    $form["content_" . $entity->id]['labelCheck_' . $entity->id] = array(
      '#type' => 'item',
      '#title' => qtici_makeLabelFeedback($form_state, $entity->id, $result),
    );

    if ($_SESSION['exercise']['attempts']['item_' . $entity->id] != 1 && $_SESSION['exercise']['attempts']['item_' . $entity->id] != NULL && !empty($entity->max_attempts) && $_SESSION['exercise']['attempts']['item_' . $entity->id] >= $entity->max_attempts) {
      $form["content_" . $entity->id]['check_answer_' . $entity->id] = array();
    }
  }

  //look if the button should be displayed
  if ($_SESSION['exercise']['show_answer_' . $entity->id] != 5 && ($testoptions->show_answer == NULL || $testoptions->show_answer == 1)) {
    //show_answers button
    $form["content_" . $entity->id]['show_answers_' . $entity->id] = array(
      '#type' => 'button',
      '#name' => 'show_answers_' . $entity->id,
      '#ajax' => array(
        'callback' => 'show_answers_ajax_callback',
        'wrapper' => 'box_' . $entity->id,
      ),
      '#value' => t('Laat het juiste antwoord zien.'),
    );
  }

  if (!empty($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'show_answers_' . $entity->id && $visibility == 0) {
    // display answer
    //display a label with answers
    if ($entity->type == 'FIB' || $entity->type == 'DAD') {
      $form["content_" . $entity->id]['labelAnswers_' . $entity->id] = array(
        '#markup' => $answerTextboxesText,
      );
    }
    else {
      $form["content_" . $entity->id]['labelAnswers_' . $entity->id] = array(
        '#markup' => $answers,
      );
    }

    $form["content_" . $entity->id]['show_answers_' . $entity->id] = array(
      '#type' => 'button',
      '#name' => 'show_answers_' . $entity->id,
      '#ajax' => array(
        'callback' => 'show_answers_ajax_callback',
        'wrapper' => 'box_' . $entity->id,
      ),
      '#value' => t('Verberg het juiste antwoord.'),
    );
  }

  $_SESSION['qtici']['visibility_' . $entity->id] = $visibility;

  $check = array_intersect(array('teacher', 'administrator'), array_values($user->roles));
  if (empty($check) ? FALSE : TRUE) {
    //end of the contextual links
    $form['contextual_menu_ends_' . $entity->id] = array(
      "#markup" => '</div>',
    );
  }

  return $form;
}

function qtici_test_entity_form($form, &$form_state, $entity) {

  drupal_add_js(array('qtici' => array('level' => $entity->level, 'topic' => $entity->topic, 'testId' => $entity->id, 'test' => TRUE)), 'setting');

  $itemids = $entity->getAllItemIDsFromAllSectionsInTest();

  $lijst = array();

  foreach ($itemids as $id) {
    $item = qtici_itemType_entity_load($id);
    $addform = qtici_item_entity_form($form, $form_state, $item);
    $lijst[$item->id] = $addform;
  }
  
  foreach ($lijst as $item_form) {
    $form += $item_form;
  }

  $form['tags'] = array(
    '#markup' => _qtici_get_tag_list($entity),
  );

  variable_set("itemids", $itemids);

  $form["timestarted"] = array(
    "#type" => "hidden",
    "#value" => time(),
  );

  /*$form["submit"] = array(
    '#type' => 'submit',
    '#value' => t('Bereken mijn score.'),
    //'#execute_submit_callback' => TRUE,
    '#weight' => 100,
  );*/

  return $form;
}

function qtici_test_entity_form_submit($form, &$form_state) {

  $itemids = variable_get("itemids");
  $test = new Test();

  $answerreturn = $test->checkTestAnswers($form, $form_state, $itemids);

  $statistic = new Statistic();
  $statistic->insertTestStatistics($form_state["build_info"]["args"][0]->id, 1, $form_state["values"]["timestarted"], $answerreturn["score"]);

  //drupal_set_message(print_r($form_state["build_info"]["args"][0]->id, true));

  drupal_set_message("Mijn score is: " . $answerreturn["score"] . " op " . $answerreturn["max"] . "!");
}

/*
 * Function to display a table with all the statistics
 */

function qtici_pageStatistics_form($form, &$form_state) {

  $form['one_test'] = array(
    '#type' => 'item',
    //'#title' => t(),
    '#markup' => t('To see individual statistics of each test, please go to the <a href="@url">test list</a> and select the corresponding button.', array('@url' => url('admin/test/manage'))),
  );

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter Tests'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  global $_qtici_levels;
  $form['filters']['level'] = array(
    '#type' => 'select',
    '#title' => t('Level'),
    '#options' => array('NO_LEVEL' => t('-- Select --')) + $_qtici_levels,
    '#default_value' => '',
    '#ajax' => array(
      'callback' => '_qtici_simple_ajax',
      'wrapper' => 'qtici_statistics_table',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  global $_qtici_topics;
  $form['filters']['topic'] = array(
    '#type' => 'select',
    '#title' => t('Topic'),
    '#options' => array('NO_TOPIC' => t('-- Select --')) + $_qtici_topics,
    '#default_value' => '',
    '#ajax' => array(
      'callback' => '_qtici_simple_ajax',
      'wrapper' => 'qtici_statistics_table',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $tags = _qtici_tag_query();
  $tag_opts = array();
  foreach ($tags as $tag) {
    $tag_opts[$tag->tid] = $tag->name;
  }

  if (!empty($tag_opts)) {
    $form['filters']['tag'] = array(
      '#type' => 'select',
      '#title' => t('Tag'),
      '#options' => array(0 => t('-- Select --')) + $tag_opts,
      '#default_value' => '',
      '#ajax' => array(
        'callback' => '_qtici_simple_ajax',
        'wrapper' => 'qtici_statistics_table',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );
  }

  $form['content'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="qtici_statistics_table">',
    '#suffix' => '</div>',
  );

  if (isset($form_state['values']) && ($form_state['values']['topic'] !== 'NO_TOPIC' || $form_state['values']['level'] !== 'NO_LEVEL' || $form_state['values']['tag'] != 0)) {
    $statisticObj = new Statistic();
    // Get the correct variables in case there is no selected
    $level = $form_state['values']['level'];
    if ($level === 'NO_LEVEL') {
      $level = '';
    }
    $topic = $form_state['values']['topic'];
    if ($topic === 'NO_TOPIC') {
      $topic = '';
    }

    $statistics = $statisticObj->getAllStatistics($level, $topic, $form_state['values']['tag']);
  }
  else {
    // By default show all statistics
    $statisticObj = new Statistic();
    $statistics = $statisticObj->getAllStatistics();
  }

  $form['content']['statisticsTable'] = array(
    '#markup' => _qtici_generalStatistics($statistics),
  );

  return $form;
}

//make the edit form for the item entity
function qtici_item_entity_edit($form, &$form_state, $entity) {

  $form['item'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#title' => t('id'),
    '#required' => TRUE,
    '#default_value' => $entity->id,
  );

  $form['sectionid'] = array(
    '#type' => 'hidden',
    '#required' => TRUE,
    '#default_value' => $entity->sectionid,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $entity->title,
  );

  $description = unserialize($entity->description);

  $form['description'] = array(
    '#type' => 'text_format',
    '#title' => t('Description'),
    '#required' => FALSE,
    '#default_value' => ($description['value']) ? $description['value'] : '',
    '#format' => ($description['format']) ? $description['format'] : 'full_html',
  );

  global $_qtici_exercisesTypes;

  $form['type'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#title' => t('Type'),
    '#description' => t(''),
    '#default_value' => $entity->type,
    '#options' => $_qtici_exercisesTypes,
  );

  $form['quotation'] = array(
    '#type' => 'radios',
    '#required' => FALSE,
    '#title' => t('Quotation'),
    '#description' => t(''),
    '#default_value' => $entity->quotation,
    '#options' => array(
      'NULL' => t('N.V.T.'),
      'perAnswer' => t('Score per antwoord'),
      'allCorrect' => t('Allemaal juist')),
  );

  $form['question'] = array(
    '#type' => 'hidden',
    '#required' => FALSE,
    '#default_value' => $entity->question,
  );

  $form['max_attempts'] = array(
    '#type' => 'textfield',
    '#title' => t('Max. Attempts'),
    '#required' => FALSE,
    '#default_value' => $entity->max_attempts,
  );

  $form['ordering'] = array(
    '#type' => 'radios',
    '#title' => t('Ordering'),
    '#required' => FALSE,
    '#description' => t(''),
    '#default_value' => $entity->ordering,
    '#options' => array(
      '1' => t('geordend'),
      '0' => t('ongeordend')),
  );

  $form['score'] = array(
    '#type' => 'textfield',
    '#title' => t('Score'),
    '#required' => FALSE,
    '#default_value' => $entity->score,
  );

  $counter = 0;
  $possibilities = _qtici_loadPossibilitiesByItemID($entity->id);

  foreach ($possibilities as $value) {

    $counter++;
    $answer = unserialize($value->answer);

    $form['possibility' . $value->id] = array(
      '#type' => 'text_format',
      '#title' => t('Mogelijkheid ' . $counter),
      '#required' => TRUE,
      '#default_value' => $answer['value'],
      '#format' => $answer['format'],
    );
  }

  field_attach_form('qtici_item', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('qtici_item_form_submit'),
    '#weight' => 100,
  );

  return $form;
}

//make the edit form for the test entity
function qtici_test_entity_edit_form($form, &$form_state, $entity) {

  $form['test'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#required' => TRUE,
    '#default_value' => $entity->id,
  );

  $form['olat_testid'] = array(
    '#type' => 'hidden',
    '#required' => TRUE,
    '#default_value' => $entity->olat_testid,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $entity->title,
  );

  //check if the value is serialized
  $description = unserialize($entity->description);

  $form['description'] = array(
    '#type' => 'text_format',
    '#title' => t('Description'),
    '#required' => FALSE,
    '#default_value' => ($description['value']) ? $description['value'] : '',
    '#format' => ($description['format']) ? $description['format'] : 'full_html',
  );

  $form['duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Duration'),
    '#required' => FALSE,
    '#default_value' => $entity->duration,
  );

  $form['passing_score'] = array(
    '#type' => 'textfield',
    '#title' => t('Passing Score'),
    '#required' => FALSE,
    '#default_value' => $entity->passing_score,
  );

  $form['published'] = array(
    '#type' => 'radios',
    '#title' => t('Published'),
    '#required' => FALSE,
    '#description' => t(''),
    '#default_value' => $entity->published,
    '#options' => array(
      '1' => t('Published'),
      '0' => t('Unpublished')),
  );

  $form['answers_in'] = array(
    '#type' => 'radios',
    '#title' => t('Toon antwoorden en feedback in de test:'),
    '#required' => FALSE,
    '#description' => t(''),
    '#default_value' => $entity->answers_in,
    '#options' => array(
      '1' => t('Ja'),
      '0' => t('Nee')),
  );

  $form['answers_out'] = array(
    '#type' => 'radios',
    '#title' => t('Toon antwoorden en feedback na de test:'),
    '#required' => FALSE,
    '#description' => t(''),
    '#default_value' => $entity->answers_out,
    '#options' => array(
      '1' => t('Ja'),
      '0' => t('Nee')),
  );

  $form['show_answer'] = array(
    '#type' => 'radios',
    '#title' => t('Toon de knop "Laat het juiste antwoord zien.":'),
    '#required' => FALSE,
    '#description' => t(''),
    '#default_value' => $entity->show_answer,
    '#options' => array(
      '1' => t('Ja'),
      '0' => t('Nee')),
  );

  $form['check_answer'] = array(
    '#type' => 'radios',
    '#title' => t('Toon de knop "Controleer mijn antwoord.":'),
    '#required' => FALSE,
    '#description' => t(''),
    '#default_value' => $entity->check_answer,
    '#options' => array(
      '1' => t('Ja'),
      '0' => t('Nee')),
  );

  $form['course'] = array(
    '#type' => 'textfield',
    '#title' => t('Course'),
    '#required' => FALSE,
    '#default_value' => $entity->course,
  );

  global $_qtici_topics;

  $form['topic'] = array(
    '#type' => 'select',
    '#title' => t('Topic'),
    '#options' => $_qtici_topics,
    '#required' => TRUE,
    '#default_value' => $entity->topic,
  );

  $form['level'] = array(
    '#type' => 'radios',
    '#title' => t('Level":'),
    '#required' => FALSE,
    '#default_value' => $entity->level,
    '#options' => array(
      'NULL' => t('N.V.T.'),
      'A' => t('A'),
      'B' => t('B'),
      'C' => t('C')),
  );

  field_attach_form('qtici_test', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('qtici_test_form_submit'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Delete Test submit
 */
function _qtici_deleteTest_submit($form, &$form_state) {
  $tests = $form_state['values']['overview_table'];

  $feedbackObj = new Feedback();

  // Prepare array
  $ids = array();
  $sectionIDS = array();
  $itemIDS = array();
  $possibilityIDS = array();

  $testObjs = qtici_test_entity_load_multiple($tests);
  foreach ($testObjs as $testObj) {
    $ids[] = $testObj->id;
    $sectionIDS[] = $testObj->getAllSectionIDsByTest();
    $itemIDS[] = $testObj->getAllItemIDsFromAllSectionsInTest();
    $possibilityIDS = $testObj->allPossibilityIDSFromAllItemsFromAllSectionsInTest($testObj->id);
    $testObj->deleteStatistic();
  }

  foreach ($itemIDS[0] as $itemID) {
    $feedbackObj->deleteFeedbackByItemID($itemID);
  }

  entity_delete_multiple('qtici_possibility', $possibilityIDS);
  entity_delete_multiple('qtici_item', $itemIDS[0]);
  entity_delete_multiple('qtici_test', $ids);

  _qtici_deleteSections($sectionIDS);

  drupal_set_message(t('Tests have been deleted'));
}

/**
 * Unpublish or publish tests
 */
function _qtici_publishTest_submit($form, &$form_state) {
  $tests = $form_state['values']['overview_table'];
  $ids = array();
  foreach ($tests as $testid) {
    if ($testid != 0) {
      $ids[] = (int) $testid;
    }
  }

  _qtici_publishTest($ids);
}

/**
 * List Tests
 */
function qtici_test_entity_list_entities() {
  $content = array();
  // Load all of our entities.
  $entities = entity_load_multiple_by_name('qtici_test');
  if (!empty($entities)) {
    foreach ($entities as $entity) {
      // Create tabular rows for our entities.
      $rows[] = array(
        'data' => array(
          'id' => $entity->id,
          'title' => l($entity->title, 'qtici/test/' . $entity->id),
        ),
      );
    }
    // Put our entities into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Title'),),
    );
  }
  else {
    // There were no entities. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No test entities currently exist.'),
    );
  }

  return $content;
}
