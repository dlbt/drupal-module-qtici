<?php

/**
 * @file - All functions that return HTML strings go here
 */
/*
 * This function will collect all tests in a datatable.
 * If the user is a teacher --> more permissions here
 */
function get_exercise_overview($rowsPerPageExtern, $titleExtern) {

  //Table of the exercises
  //objects of the classes statistic and test for accessing their methods
  $statistic = new Statistic();
  $rowsPerPage = $rowsPerPageExtern;

  //look if a title is given and when not make it blank
  $title = '';
  if ($titleExtern != '') {
    $title = $titleExtern;
  }

  //Create a list of headers for your Html table (see Drupal 7 docs for theme_table here: http://api.drupal.org/api/drupal/includes--theme.inc/function/theme_table/7
  $header = array(
    'title' => array('data' => t('Titel'), 'field' => 'title', 'sort' => 'asc'),
    'description' => array('data' => t('Omschrijving'), 'field' => 'description', 'sort' => 'asc'),
    'course' => array('data' => t('Cursusnaam'), 'field' => 'course', 'sort' => 'asc'),
    'date' => array('data' => t('Datum'), 'field' => 'date', 'sort' => 'asc'),
    'others' => array('data' => t('Opties')),
  );

  //Create the Sql query.
  $query = db_select('qtici_test', 't')->extend('PagerDefault')->limit($rowsPerPage);
  $query->fields('t')->extend("TableSort")->orderByHeader($header);

  //only get the published ones if the user is a student
  if (!(user_access('teacher'))) {
    $query->condition('published', 1, '=');
  }

  //search for the titel if given
  if ($title) {
    $query->condition('title', "%" . $title . "%", 'LIKE');
  }

  //execute the query
  $results = $query->execute();

  //image variables
  $variables = array(
    "path" => drupal_get_path('module', 'qtici') . "/css/images/detail16.png",
    "attributes" => '',
  );

  //images
  $image1 = theme_image($variables);

  //create rows of table
  $rows = array();
  $counter = 0;
  $html = '';
  $options = array();

  //make the records of the table
  foreach ($results as $node) {

    //increase counter for PastURL script
    $counter++;

    //look which icons should be displayed
    if (user_access('teacher')) {

      $statExists = $statistic->checkIfStatisticsExist($node->id);
      if ($statExists) {
        $path2 = drupal_get_path('module', 'qtici') . "/css/images/statistics.png";
      }
      else {
        $path2 = drupal_get_path('module', 'qtici') . "/css/images/nostatistics.png";
      }

      $variables2 = array(
        "path" => $path2,
        "attributes" => '',
      );

      $variables3 = array(
        "path" => drupal_get_path('module', 'qtici') . "/css/images/past.png",
        "attributes" => '',
      );

      if ($node->published == 1) {
        $path4 = drupal_get_path('module', 'qtici') . "/css/images/published.png";
      }
      else {
        $path4 = drupal_get_path('module', 'qtici') . "/css/images/unpublished.png";
      }

      $variables4 = array(
        "path" => $path4,
        "width" => 16,
        "height" => 16,
        "attributes" => '',
      );

      $image2 = theme_image($variables2);
    }

    //check if the value is serialized
    $array = @unserialize($node->description);
    if ($array === false && $node->description !== 'b:0;') {

      $options[$node->id] = array(
        'title' => l(t((string) $node->title), 'qtici/test/' . $node->id),
        //l(t((string) $node->title), 'oefeningen/view', array('query' => array("testid" => $node->id))),
        'description' => $node->description,
        'course' => $node->course,
        'date' => _qtici_getNiceDate($node->date),
        'others' => t(''),
      );
    }
    else {
      //unserialize the description so you can show the value
      $descriptionUnserialized = unserialize($node->description);

      $options[$node->id] = array(
        'title' => l(t((string) $node->title), 'qtici/test/' . $node->id),
        'description' => $descriptionUnserialized["value"],
        'course' => $node->course,
        'date' => _qtici_getNiceDate($node->date),
        'others' => t(''),
      );
    }

    if (user_access('teacher')) {
      $options[$node->id]['others'] = l($image1, 'qtici/test/' . $node->id, array('attributes' => array('class' => 'img_display'), 'html' => TRUE)) . l($image2, 'qtici/statistics/' . $node->id, array('attributes' => array('class' => 'img_statistics'), 'html' => TRUE));
      //l($image3, 'oefeningen/copy', array('attributes' => array('class' => array('img_past'), 'html' => TRUE)) 
    }
  }
  
  $args = array('quantity' => $rowsPerPage, 'tags' => array('<<', '<', '', '>', '>>'));
  $html .= theme('pager', $args);

  return array('html' => $html, 'header' => $header, 'options' => $options);
}

function get_exercise_overview_mobile($rowsPerPageExtern, $titleExtern) {

  //Table of the exercises
  //objects of the classes statistic and test for accessing their methods
  $rowsPerPage = $rowsPerPageExtern;

  //look if a title is given and when not make it blank
  $title = '';
  if ($titleExtern != '') {
    $title = $titleExtern;
  }

  //Create a list of headers for your Html table (see Drupal 7 docs for theme_table here: http://api.drupal.org/api/drupal/includes--theme.inc/function/theme_table/7
  $header = array(
    'title' => array('data' => t('Titel'), 'field' => 'title', 'sort' => 'asc'),
  );

  //Create the Sql query.
  $query = db_select('qtici_test', 't')->extend('PagerDefault')->limit($rowsPerPage);
  $query->fields('t')->extend("TableSort")->orderByHeader($header);

  //only get the published ones if the user is a student
  if (!(user_access('teacher'))) {
    $query->condition('published', 1, '=');
  }

  //search for the titel if given
  if ($title) {
    $query->condition('title', "%" . $title . "%", 'LIKE');
  }

  //execute the query
  $results = $query->execute();

  //create rows of table
  $html = '';
  $options = array();

  //make the records of the table
  foreach ($results as $node) {

      $options[$node->id] = array(
        'title' => l(t((string) $node->title), 'qtici/test/' . $node->id),
      );
      
  }
  
  $args = array('quantity' => $rowsPerPage, 'tags' => array('<<', '<', '', '>', '>>'));
  $html .= theme('pager', $args);

  return array('html' => $html, 'header' => $header, 'options' => $options);
}

/**
 * Display Page statistics
 */
function theme_qtici_statistics($variables) {

  $header = $variables['header'];
  $rows = $variables['rows'];
  $html = '';

  if (!empty($rows)) {
    $html .= '<table>';
    foreach ($header as $key => $value) {
      $html .= '<tr><th style="width:175px">' . $header[$key]['data'] . '</th><td>' . $rows[0][$key] . '</td></tr>';    
    }
    $html .= '</table>';
  }
  else {
    if (!empty($variables['title'])) {
      $html .= t('No statistics for !name', array('!name' => $variables['title']));
    }
  }

  return $html;
}

/**
 * Display statistics for one test
 */
function _qtici_testStatistics($test) {
  //Prepare table
  $header = array(
    'title' => array('data' => t('Titel'), 'field' => 'title'),
    'description' => array('data' => t('Omschrijving'), 'field' => 'description'),
    'completed' => array('data' => t('Aantal'), 'field' => 'completed'),
    'time' => array('data' => t('Gem. tijd (in sec.)')),
    'score' => array('data' => t('Score'), 'field' => 'score'),
    'max_score' => array('data' => t('Max. score')),
    'date' => array('data' => t('Datum laatst gestart'), 'field' => 'date_started'),
  );
  $rows = array();
  $title = '';
  
  $title = l($test->title, 'qtici/test/' . $test->id);
  //objects of the classes statistic and test for using their functions
  $statisticObj = new Statistic();
    
  $statistic = $statisticObj->getAllStatisticsByTestID($test->id);
  if (!empty($statistic)) {

    $timespended = (float) $statistic['time_spended'];
    $timespended /= 1000;
    $timespended = round($timespended, 2);
    $description = $statistic['description'];

    $rows[] = array(
      'title' => l($statistic['title'], 'qtici/test/' . $test->id),
      'description' => $description,
      'completed' => $statistic['completed'],
      'time' => $timespended,
      'score' => $statistic['score'],
      'max_score' => $test->getMaximumScoreTest(),
      'date' => _qtici_getNiceDate($statistic['date_started']),
    );
  }
  
  $output = theme('qtici_statistics', array('header' => $header, 'rows' => $rows, 'title' => $title));
  
  return $output;
}

/**
 * Display statistics for a group of tests
 */
function _qtici_generalStatistics($statistics) {
  //Prepare table
  $header = array(
    'numberTests' => array('data' => t('Total number of tests')),
    'published' => array('data' => t('Total number of published tests')),
    'neverDone' => array('data' => t('Tests that never have been done')),
    'durationTot' => array('data' => t('Total duration of the tests')),
    'time_spended_tot' => array('data' => t('Total time spent (in sec.)')),
    'completed' => array('data' => t('Tests completed')),
    'totalScore' => array('data' => t('Total score over total maximum score')),
    'durationAverage' => array('data' => t('Average duration of the tests')),
    'time_spended' => array('data' => t('Average spent time')),
    'scoreAverage' => array('data' => t('Average score')),
    'date_started' => array('data' => t('Date statistics started')),
  );
  $rows = array();

  $testObj = new Test();

  // Fix times and dates
  $timespended_tot = (float) $statistics['time_spended_tot'];
  $timespended_tot /= 1000;
  $timespended_tot = round($timespended_tot, 2);
  $statistics['time_spended_tot'] = $timespended_tot;
  $timespended = (float) $statistics['time_spended'];
  $timespended /= 1000;
  $timespended = round($timespended, 2);
  $statistics['time_spended'] = $timespended;
  $statistics['date_started'] = _qtici_getNiceDate($statistics['date_started']);
  $statistics['totalScore'] = $statistics['totalScore'] . '/' . $statistics['max_score'];

  foreach ($header as $key => $value) {
    $rows[0][$key] = $statistics[$key];
  }

  $output = theme('qtici_statistics', array('header' => $header, 'rows' => $rows));
  
  return $output;
}
