<?php

class Item extends Entity {

  //name variables same as database columns
  //  protected $ident;
  public $type;
  public $title;
  public $objective;
  public $feedback = array();
  public $hint;
  public $solutionFeedback;
  public $max_attempts;
  public $possibilities = array();
  public $question;
  public $id;
  public $content;
  public $sectionid;
  public $description;

  public function __construct($values = array()) {
    parent::__construct($values, 'item');
  }

  public function myConstruct($ident, $type, $title, $objective = null, $max_attempts = '') {
    $this->id = $ident;
    $this->type = $type;
    $this->title = $title;
    //$this->objective = $objective;
    $this->max_attempts = $max_attempts;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($string) {
    $description = array(
      'value' => $string,
      'format' => 'full_html',
    );
    $this->description = serialize($description);
  }

  public function getSectionid() {
    return $this->sectionid;
  }

  public function setSectionid($sectionid) {
    $this->sectionid = $sectionid;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getId() {
    return $this->id;
  }

  public function getType() {
    return $this->type;
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function getQuestion() {
    return $this->question;
  }

  public function setQuestion($question) {
    $this->question = $question;
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function getTitle() {
    return $this->title;
  }

  public function setObjective($objective) {
    $this->objective = $objective;
  }

  public function getObjective() {
    return $this->objective;
  }

  public function setFeedback($feedback) {
    array_push($this->feedback, $feedback);
  }

  public function getFeedback() {
    return $this->feedback;
  }

  public function setHint($hint) {
    $this->hint = $hint;
  }

  public function getHint() {
    return $this->hint;
  }

  public function setSolutionFeedback($solutionFeedback) {
    $this->solutionFeedback = $solutionFeedback;
  }

  public function getSolutionFeedback() {
    return $this->solutionFeedback;
  }

  public function getMax_attempts() {
    return $this->max_attempts;
  }

  public function setMax_attempts($max_attempts) {
    $this->max_attempts = $max_attempts;
  }

  public function setPossibility($possibilities) {
    array_push($this->possibilities, $possibilities);
  }

  public function getPossibilities() {
    return $this->possibilities;
  }

  public function deletePossibilities() {
    $this->possibilities = array();
  }

  public function setContent($content) {
    $this->content = serialize($content);
  }

  public function getContent() {
    return unserialize($this->content);
  }

  /**
   * Queries of this class
   */

  public function getQuestionByItem() {

    $query = db_select('qtici_item', 'i');
    $query->fields('i', array('question'));
    $query->condition('i.id', $this->id, '=');
    $result = $query->execute();

    return $result;
  }

  public function parseXML($item) {
    $this->setId((string) getDataIfExists($item, 'attributes()', 'ident'));
    $this->setTitle((string) getDataIfExists($item, 'attributes()', 'title'));
    $this->setObjective((string) getDataIfExists($item, 'objectives', 'material', 'mattext'));
    $this->setDescription((string) getDataIfExists($item, 'objectives', 'material', 'mattext'));

    qtici_fetchFeedback($this, $item);
  }

  /**
   * Checks if answer (defined per exercise)
   */
  public function checkAnswer($form_state) {
    $returnArray = array();
    return $returnArray;
  }
  
  /**
   * Checks answer to return test score (defined per exercise)
   */
  public function checkAnswerForTest($form_state) {
    return $this->checkAnswer($form_state);
  }
  
  /**
   * Creates exercise form
   */
  public function makeExerciseForm(&$info, &$options = array(), &$possibilities = array()) {
    
    //insert the possibilities and answers for that item in the options array
    $options = array();
    $possibilities = _qtici_loadPossibilitiesByItemID($this->id);

    //make the options and answers
    foreach ($possibilities as $posibility) {

      //process the blob value
      $answerArray = unserialize($posibility->answer);

      //insert al the possible options
      $options += array($posibility->id => $answerArray["value"]);
    }

    $info = _qtici_checkMedia($this->question, $this->id);
    //Make the question/description of the exercise
    $itemDescription = '';
    if (!empty($this->description)) {
      $dumbVar = unserialize($this->description);
      // Don't know if this is necessary since we save the format in the db
      $itemDescription = '<div style="margin:10px" class="img_info">' . $dumbVar['value'] . '</div>';
    }

    //label for showing the title and the discription
    $form['item_info_' . $this->id] = array(
      '#markup' => '<h2 style="margin-bottom: 15px">' . $this->title . '</h2>' . '<span style="padding-left: 5px">' . $itemDescription . '</span>',
    );
    
    return $form;
  }
}
