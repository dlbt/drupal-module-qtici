<?php

class MultipleChoiceQuestion extends Item {

  public $quotation;
  public $answers = array();
  public $score;
  public $randomOrder;

  public function __construct($values = array()) {
    parent::__construct($values, 'qtici_MCQ');
  }

  function myFullConstruct($item) {
    $this->type = $item->type;
    $this->title = $item->title;
    $this->objective = NULL;
    $this->feedback = NULL;
    $this->hint = NULL;
    $this->solutionFeedback = NULL;
    $this->max_attempts = $item->max_attempts;
    $this->possibilities = NULL;
    $this->question = $item->question;
    $this->id = $item->id;
    $this->quotation = $item->quotation;
    $this->answers = NULL;
    $this->score = $item->score;
    $this->randomOrder = $item->ordering;
  }

  public function setQuotation($quotation) {
    $this->quotation = $quotation;
  }

  public function getQuotation() {
    return $this->quotation;
  }

  public function setAnswer($answers) {
    array_push($this->answers, $answers);
  }

  public function getAnswer() {
    return $this->answers;
  }

  public function setScore($score) {
    $this->score = $score;
  }

  public function getScore() {
    return $this->score;
  }

  public function setRandomOrder($randomOrder) {
    if ($randomOrder == 'Yes') {
      $this->randomOrder = TRUE;
    }
    else {
      $this->randomOrder = FALSE;
    }
  }

  public function getRandomOrder() {
    return $this->randomOrder;
  }

  /**
   * Functions of this class
   */

  /**
   * Check the answer of an MCQ question
   */
  public function checkAnswer($form_state) {

    //get the item id
    $itemid = $this->getId();
    
    $returnArray = array();

    //get the checked answers
    $checkboxAnswers = array_filter($form_state['values']['item_' . $itemid]);
    $checkboxAnswersUserText = NULL;
    $poss = _qtici_getCorrectPossibilityForItem($itemid);
    $userAnswer = array();

    //loop through the given checked answers
    foreach ($checkboxAnswers as $checkboxAnswer) {

      //get the answer of the user out of the database
      $userPoss = db_select('qtici_possibility', 'p')
          ->fields('p')
          ->condition('p.id', $checkboxAnswer)
          ->execute()
          ->fetchAll();

      //unserialize the answer
      $unserializedAnswer = unserialize($userPoss[0]->answer);
      $userAnswer[] = $unserializedAnswer["value"];
      $checkboxAnswersUserText = $unserializedAnswer['value'] . ' ';
    }

    $counter = 0;
    $score = 0;
    $anwersUser = explode(" ", $checkboxAnswersUserText);
    $anwersUser = array_filter($anwersUser);

    //get the score of ge user,
    if ($poss == $userAnswer) {
      $returnArray["trueFalse"] = TRUE;
      $returnArray["score"] = $this->score;
    }
    else {
      if (count($poss) >= count($userAnswer)) {
        $result = array_diff($poss, $userAnswer);
        $score = count(array_diff($poss, $result)) / count($poss);
      }
      else {
        $result = array_diff($userAnswer, $poss);
        $score = $this->score - count($result)/ count($poss);
      }
      $returnArray["trueFalse"] = FALSE;
      $returnArray["score"] = $score;
    }

    $returnArray['numberOfTextboxes'] = 0;

    return $returnArray;
  }

  //public function checkAnswerForTest($form, $form_state) {
    
    //$answers = $form_state["input"]['item_' . $this->id];
    //$correctanswer = $form_state["input"]['answer_' . $this->id];
    //$itemscore = $this->score;
    ////get the checked answers
    //$checkboxAnswers = array_filter($answers);
    //$checkboxAnswersUserText = NULL;

    //$correctanswerexplosion = explode(" ", $correctanwser);

    //$scoretoadd = count($correctanswerexplosion) / $itemscore;

    //$score = 0;
    
    //$returnarray = array();

    ////loop through the given checked answers
    //foreach ($checkboxAnswers as $checkboxAnswer) {

      ////get the answer of the user out of the database
      //$userAnswer = db_select('qtici_possibility', 'p')
          //->fields('p')
          //->condition('p.id', $checkboxAnswer)
          //->execute()
          //->fetchAll();

      ////unserialize the answer
      //$unserializedAnswer = unserialize($userAnswer[0]->answer);

      ////set the test of the checked answer in a string
      //$checkboxAnswersUserText .= $unserializedAnswer["value"] . " ";

      //foreach ($correctanswerexplosion as $answer) {
        //if ($answer == $unserializedAnswer["value"]) {
          //$score += $scoretoadd;
        //}
      //}
    //}

    //$returnarray["score"] = $score;

    ////look if the answer is correct
    //if (strcmp(trim($correctanwser), trim($checkboxAnswersUserText)) == 0) {
      //$returnarray["truefalse"] = TRUE;
    //}
    //else {
      //$returnarray["truefalse"] = FALSE;
    //}
    
    //$returnArray['numberOfTextboxes'] = 0;

    //return $returnarray;
  //}

  /**
   * Make multi choice exercise form
   */
  function makeExerciseForm(&$info, &$options = array(), &$possibilities = array()) {
    
    $form = parent::makeExerciseForm($info, $options);
    $form['item_question' . $this->id] = array(
      '#markup' => htmlspecialchars_decode($this->question),
    );

    $form['item_' . $this->id] = array(
      '#type' => 'checkboxes',
      '#required' => FALSE,
      '#title' => t(''),
      '#title_display' => 'invisible',
      '#options' => $options,
      '#theme' => 'qtici_item_form_checkboxes',
    );

    return $form;
  }

  /**
   * Parser function. $item is the loaded XML object
   */
  public function parseXML($item) {
    $this->setRandomOrder((string) getDataIfExists($item, 'presentation', 'response_lid', 'render_choice', 'attributes()', 'shuffle'));
    // Set Type
    $this->setType('MCQ');
    // Get Quotation
    $outputArray = getQuotationType($item);
    $quotation = $outputArray['quotation'];
    $results = $outputArray['results'];

    $this->setQuotation($quotation);

    // Get correct answers
    $correct = array();
    foreach ($item->resprocessing->respcondition as $resp) {
      if ($resp->attributes()->title == 'Mastery') {
        if (getDataIfExists($resp, 'conditionvar', 'and', 'varequal')) {
          foreach ($resp->conditionvar->and->varequal as $varequal) {
            $correct[] = (int) getDataIfExists($varequal);
          }
        }
        else {
          foreach ($resp->conditionvar->varequal as $varequal) {
            $correct[] = (int) getDataIfExists($varequal);
          }
        }
      }
    }
 
    // Get answers
    foreach ($item->presentation->response_lid->render_choice->children() as $child) {
      $possibility = new Possibility();
      $content['value'] = (string) getDataIfExists($child, 'response_label', 'material', 'mattext');
      $content['format'] = (string) getDataIfExists($child, 'response_label', 'material', 'mattext', 'texttype');
      if (empty($content['format'])) {
        $content['format'] = 'full_html';
      }
      $ident = (int) getDataIfExists($child, 'response_label', 'attributes()', 'ident');
      $is_correct = 0;
      if (in_array($ident, $correct)) {
        $is_correct = 1;
      }
      $possibility->myConstruct(NULL, $ident, ElementTypes::CHECKBOX, NULL, serialize($content), NULL, $is_correct, NULL);
      $this->setPossibility($possibility);
    }

    // Set Score
    $answers = array();
    if ($quotation == 'allCorrect') {
      foreach ($results as $result) {
        $arrayAnswer = $result->conditionvar->and;
        if (count($arrayAnswer->varequal) != 0) {
          for ($i = 0; $i < count($arrayAnswer->varequal); $i++) {
            $title = (string) $result->attributes()->title;
            $answers[$title] = (string) $arrayAnswer->varequal[$i];
          }
        }
        $this->setScore((string) $results[0]->setvar);
      }
    }
    elseif ($quotation == 'perAnswer') {
      foreach ($results as $result) {
        $answers[$result->attributes()->title] = array(
          'value' => (string) $result->conditionvar->varequal,
          'score' => (string) $result->setvar,
        );
      }
    }
    $this->setAnswer($answers);

    parent::parseXML($item);
  }

}

?>
