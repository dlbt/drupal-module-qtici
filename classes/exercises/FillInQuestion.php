<?php

class FillInQuestion extends Item {

  public $quotation;
  public $answers = array();
  public $score;

  public function __construct($values = array()) {
    parent::__construct($values, 'qtici_FIB');
  }

  function myFullConstruct($item) {
    $this->type = $item->type;
    $this->title = $item->title;
    $this->objective = NULL;
    $this->feedback = NULL;
    $this->hint = NULL;
    $this->solutionFeedback = NULL;
    $this->max_attempts = $item->max_attempts;
    $this->possibilities = NULL;
    $this->question = $item->question;
    $this->id = $item->id;
    $this->quotation = $item->quotation;
    $this->answers = NULL;
    $this->score = $item->score;
    $this->quotation = $item->quotation;
  }

  public function setQuotation($quotation) {
    $this->quotation = $quotation;
  }

  public function getQuotation() {
    return $this->quotation;
  }

  public function setAnswer($answers) {
    array_push($this->answers, $answers);
  }

  public function getAnswer() {
    return $this->answers;
  }

  public function setScore($score) {
    $this->score = $score;
  }

  public function getScore() {
    return $this->score;
  }

  /**
   * Functions of this class
   */

  protected function cleanString($string) {
    $string = trim($string);
    $search = array( "á", "à", "â", "ä", "é", "è", "ê", "í", "ì", "î", "ó", "ò", "ö", "ô", "ú", "ù", "û", "ü" );
    $replace = array( "a", "a", "a", "a", "e", "e", "e", "i", "i", "i", "o", "o", "o", "o", "u", "u", "u", "u");
    $string = preg_replace("/(?![.=$'€%-])\p{P}/u", "", $string);
    $string = strtolower($string);
    $string = str_replace($search, $replace, $string);

    return $string;
  }

  /**
   * Check the answer of an FIB question
   */
  public function checkAnswer($form_state) {

    //get the item id
    $itemid = $this->getId();

    //get the ids from the textboxes
    $textboxIDS = explode("/", $form_state['values']['texboxenFIBDAD_' . $itemid]);
    $correctAnswers = explode("/", trim($form_state['values']['answerTextboxes_' . $itemid]));

    //get rid of the empty values in the array
    $textboxIDS = array_filter($textboxIDS);
    $correctAnswers = array_filter($correctAnswers);

    //initiatlize string for putting in the vaues of the textboxes
    $answerUser = NULL;
    $counter = 0;
    $score = 0;

    //initialize a color
    drupal_add_js(array('qtici' => array('qtici_textbox' => 0, 'qtici_color' => "white")), 'setting');

    //run through all the ids and get the value of the textbox
    foreach ($textboxIDS as $textboxID) {
      $answerUser = trim($_POST['textbox_' . $textboxID]);
      
      $correctNum = 0;
      
      if (isset($correctAnswers[$counter]) && strcmp($this->cleanString($answerUser), $this->cleanString($correctAnswers[$counter])) == 0) {
         $correctNum = 1;
      }

      if ($correctNum == 1) {
        drupal_add_js(array('qtici' => array('qtici_textbox' => $textboxID, 'qtici_color' => "green")), 'setting');
        $score++;
      }
      else {
        drupal_add_js(array('qtici' => array('qtici_textbox' => $textboxID, 'qtici_color' => "red")), 'setting');
      }

      $counter++;
    }

    // Call the JS here
    drupal_add_js(drupal_get_path('module', 'qtici') . '/js/textboxColorChanger.js');

    $returnArray["numberOfTextboxes"] = count($textboxIDS);
    $returnArray["score"] = $score;

    //look if the answer is correct
    if (count($textboxIDS) == $score) {
      $returnArray["trueFalse"] = TRUE;
    }
    else {
      $returnArray["trueFalse"] = FALSE;
    }

    return $returnArray;
  }

  //public function checkAnswerForTest($form, $form_state) {

    //$boxids = $form_state["input"]['texboxenFIBDAD_' . $this->id];
    //$answers = $form_state["input"]['answerTextboxes_' . $this->id];
    ////get the ids from the textboxes
    //$textboxIDS = explode(",", $boxids);
    //$correctAnswers = explode(" ", trim($answers));

    ////get rid of the empty values in the array
    //$textboxIDS = array_filter($textboxIDS);
    //$correctAnswers = array_filter($correctAnswers);

    ////initiatlize string for putting in the vaues of the textboxes
    //$answerUser = NULL;
    //$counter = 0;

    //$numbercorrect = 0;

    ////run through all the ids and get the value of the textbox
    //foreach ($textboxIDS as $textboxID) {
      //$answerUser .= trim($_POST['textbox_' . $textboxID]) . " ";

      //if (strcmp(trim($_POST['textbox_' . $textboxID]), trim($correctAnswers[$counter])) == 0) {
        //drupal_add_js(array('qtici' => array('qtici_textbox' => $textboxID, 'qtici_color' => "green")), 'setting');
        //$numbercorrect += 1;
      //}
      //else {
        //drupal_add_js(array('qtici' => array('qtici_textbox' => $textboxID, 'qtici_color' => "red")), 'setting');
      //}

      //$counter++;
    //}

    //// Call the JS here
    //drupal_add_js(drupal_get_path('module', 'qtici') . '/js/textboxColorChanger.js');

    //$returnArray["numberOfTextboxes"] = $counter;
    //$returnarray["score"] = $numbercorrect;

    ////look if the answer is correct
    //if (strcmp(trim($answers), trim($answerUser)) == 0) {
      //$returnarray["truefalse"] = true;
    //}
    //else {
      //$returnarray["truefalse"] = false;
    //}

    //return $returnarray;
  //}

  /**
   * Make FIB exercise form
   */
  function makeExerciseForm(&$info, &$options = array(), &$possibilities = array()) {
    
    $form = parent::makeExerciseForm($info);
    
    // Get content (field should change name to avoid this)
    $content = db_select('qtici_item', 'i')
            ->fields('i', array(
              'content',
            ))
            ->condition('id', $this->id, '=')
            ->execute()->fetchField();

    $content = unserialize($content);
    //Decode and encode to get rid of weird symbols
    $content = htmlentities($content);
    $content = html_entity_decode($content, ENT_COMPAT, 'UTF-8');
    $info = _qtici_checkMedia($content, $this->id);
    _qtici_checkTextbox($content);
    
    $form['question' . $this->id] = array(
      '#markup' => $content . "<br /><br />",
    );

    return $form;
  }

  public function parseXML($item) {
    // Set Type
    $this->setType('FIB');
    // Get Quotation
    $outputArray = getQuotationType($item);
    $quotation = $outputArray['quotation'];
    $results = $outputArray['results'];

    $this->setQuotation($quotation);

    // Get Answers
    $answers = array();
    if ($quotation == 'allCorrect') {
      foreach ($results as $result) {
        foreach ($result->conditionvar->and->or as $arrayAnswer) {
          $ident = (int) $arrayAnswer->varequal->attributes()->respident;
          $answers[$ident] = (string) $arrayAnswer->varequal;
        }
        $this->setAnswer($answers);
        $this->setScore((string) $result->setvar);
      }
    }
    elseif ($quotation == 'perAnswer') {
      foreach ($results as $result) {
        $answers[(string) $result->conditionvar->or->varequal['respident']] = array(
          'value' => (string) $result->conditionvar->or->varequal,
          'score' => (string) $result->setvar,
        );
        $this->setAnswer($answers);
      }
    }

    $content = '';
    foreach ($item->presentation->flow->children() as $child) {
      // MATERIAL can have the mattext or matimage elements (text/image)
      if ($child->getName() == 'material') {
        $materialArray = $child->xpath('*');
        foreach ($materialArray as $element) {
          if ($element->getName() == 'mattext') {
            $content .= (string) $element;
          }
          if ($element->getName() == 'matimage') {
            // Save image
            $newFile = file_save_upload((string) $element['uri'], array('file_validate_extensions' => array($allowed)));
            $newFile = file_move($newFile, 'public://');
            $newFile->status = 1; // Make permanent
            $newFile = file_save($newFile);
            $content .= ':img' . $newFile->fid . 'fid:';
          }
        }
      }
      elseif ($child->getName() == 'response_str') { // TEXTBOX
        $ident = (int) getDataIfExists($child, 'attributes()', 'ident');
        $content .= ':text' . $ident . 'box:';
        $possibility = new Possibility();
        $answer = NULL;
        if ($ident && !empty($answers[$ident])) {
          if (is_object($answers[$ident])) {
            $answer = $answers[$ident]->value;
          }
          else {
            $answer = $answers[$ident];
          }
        }
        $dumbAns['value'] = $answer;
        $dumbAns['format'] = 'full_html';
        $possibility->myConstruct(NULL, (string) getDataIfExists($child, 'attributes()', 'ident'), ElementTypes::TEXTBOX, NULL, serialize($dumbAns), NULL, NULL, NULL);
        $this->setPossibility($possibility);
      }
    }
    $this->setContent(html_entity_decode($content));

    parent::parseXML($item);
  }

}
