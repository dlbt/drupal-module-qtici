<?php

class VideoQuestion extends Item {

  public $answers = array();
  public $score;

  public function __construct($values = array()) {
    parent::__construct($values, 'qtici_VID');
  }

  public function getAnswers() {
    return $this->answers;
  }

  public function setAnswers($answers) {
    $this->answers = $answers;
  }

  public function getScore() {
    return $this->score;
  }

  public function setScore($score) {
    $this->score = $score;
  }

  /**
   * Check the answer of a VID question
   */
  public function checkAnswer($form_state) {

    $returnArray = array();
    $spendedTime = $form_state["values"]["VID_hidden_" . $this->id];
    $answer_array = $this->VID_validate();
    $studentAnswerArray = explode('-', $spendedTime);
    $studentAnswerArray = array_filter($studentAnswerArray);

    $returnArray["trueFalse"] = true;
    $returnArray['numberOfTextboxes'] = count($answer_array);
    $returnArray["score"] = 1;
    $scorePiece = 1 / count($answer_array);

    for ($i = 0; $i <= count($answer_array) - 1; $i++) {

      if (isset($studentAnswerArray[$i])) {
        $simpleSpendedTime = floor($studentAnswerArray[$i]);

        $answer_item = explode('-', $answer_array[$i]);
        $begin_time = explode(',', $answer_item[0]);
        $end_time = explode(',', $answer_item[1]);
        $answer_item[0] = $begin_time[0] * 60 + $begin_time[1];
        $answer_item[1] = $end_time[0] * 60 + $end_time[1];

        if ($simpleSpendedTime < $answer_item[0] || $simpleSpendedTime > $answer_item[1]) {
          $returnArray["trueFalse"] = FALSE;
          $returnArray["score"] = $returnArray["score"] - $scorePiece;
        }
      }
      else {
        $returnArray["score"] = $returnArray["score"] - $scorePiece;
        $returnArray["trueFalse"] = FALSE;
      }
    }

    return $returnArray;
  }

  

  //public function checkAnswerForTest($form, $form_state) {

    //$returnArray = array();
    //$spendedTime = $form_state["input"]['VID_hidden_' . $this->id];
    //$answer_array = $this->VID_validate();
    //$studentAnswerArray = explode('-', $spendedTime);
    //$studentAnswerArray = array_filter($studentAnswerArray);

    //$returnArray["trueFalse"] = true;
    //$returnArray['numberOfTextboxes'] = count($answer_array);
    //$returnArray["score"] = 1;
    //$scorePiece = 1 / count($answer_array);

    //for ($i = 0; $i <= count($answer_array) - 1; $i++) {

      //if (isset($studentAnswerArray[$i])) {
        //$simpleSpendedTime = floor($studentAnswerArray[$i]);

        //$answer_item = explode('-', $answer_array[$i]);
        //$begin_time = explode(',', $answer_item[0]);
        //$end_time = explode(',', $answer_item[1]);
        //$answer_item[0] = $begin_time[0] * 60 + $begin_time[1];
        //$answer_item[1] = $end_time[0] * 60 + $end_time[1];

        //if ($simpleSpendedTime < $answer_item[0] || $simpleSpendedTime > $answer_item[1]) {
          //$returnArray["trueFalse"] = FALSE;
          //$returnArray["score"] = $returnArray["score"] - $scorePiece;
        //}
      //}
      //else {
        //$returnArray["score"] = $returnArray["score"] - $scorePiece;
        //$returnArray["trueFalse"] = FALSE;
      //}
    //}

    //return $returnArray;
  //}

  /**
   * Used by checkAnswer
   */
  private function VID_validate() {
    $result = _qtici_getCorrectPossibilityForItem($this->id);
   
    foreach ($result as $item) {
      $answer[] = str_replace(' ', '', $item);
    }

    return $answer;
  }

  /**
   * Display function for video exercises
   */
  public function makeExerciseForm(&$info, &$options = array(), &$possibilities = array()) {
    
    $form = parent::makeExerciseForm($info, $options, $possibilities);

    //make the hidden field that holds the times for checking later
    $form['VID_hidden_' . $this->id] = array(
      '#type' => 'hidden',
      '#attributes' => array('id' => "VID_hidden_" . $this->id),
    );

    //make the question
    $form['VID_' . $this->id] = array(
      '#type' => 'item',
      '#title' => htmlspecialchars_decode($this->question),
    );

    //make the button for getting the current time
    $form["VID_getTime_" . $this->id] = array(
      '#markup' => '<input type="button" value="' . t('Grijp tijd') . '" id = "get_time_player_' . $this->id . '" name = "grip_button" class = "form-submit">',
    );
    //get the table for displaying the input boxes for the times
    $table = $this->create_videoTimesTable(count($possibilities) - 1);

    //display the table
    $form['VID_table' . $this->id] = array(
      '#type' => 'item',
      '#markup' => $table,
    );
    
    $form['#attached']['library'][] = drupal_add_library('qtici', 'videoQuestion');

    return $form;
  }
  
  /**
   * make the table for the video exercise for displaying the input fields for the selected times
   */
  private function create_videoTimesTable($possibilitiesCount) {

    //make the input fields
    $inputFields = NULL;
    for ($i = 0; $i <= $possibilitiesCount; $i++) {

      //make one input field
      $inputField = array(
        '#type' => 'textfield',
        '#title' => t(''),
        '#description' => t(''),
        '#size' => 6,
        '#attributes' => array(
          'readonly' => 'readonly',
          'id' => 'inputbox_time_player_' . $this->id . '_id_' . $i,
          'style' => 'background-color:#CFF; float: left;',
          'class' => array('time_stamp_player'),
          'name' => 'inputbox_time_player_' . $this->id . '_id_' . $i,
        ),
      );

      //render the input field and add it to the others
      $inputFields .= render($inputField);
    }

    //make the rows of the table
    $options[] = array(
      'header1' => $inputFields,
      'header2' => '<input type="button" value="' . t('Opnieuw') . '" name = "qclear_button" class = "qtici_clear_button_' . $this->id . '">',
    );

    //render the table
    $html = theme('table', array(
      'rows' => $options,
      'sticky' => TRUE, //Optional to indicate whether the table headers should be sticky
      )
    );

    return $html;
  }
}

?>
