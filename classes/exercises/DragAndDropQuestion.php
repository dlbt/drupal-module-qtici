<?php

class DragAndDropQuestion extends Item {

  public $quotation;
  public $answers = array();
  public $score;

  public function __construct($values = array()) {
    parent::__construct($values, 'qtici_DAD');
  }

  function myFullConstruct($item) {
    $this->type = $item->type;
    $this->title = $item->title;
    $this->objective = NULL;
    $this->feedback = NULL;
    $this->hint = NULL;
    $this->solutionFeedback = NULL;
    $this->max_attempts = $item->max_attempts;
    $this->possibilities = NULL;
    $this->question = $item->question;
    $this->id = $item->id;
    $this->quotation = $item->quotation;
    $this->answers = NULL;
    $this->score = $item->score;
    $this->quotation = $item->quotation;
  }

  public function setQuotation($quotation) {
    $this->quotation = $quotation;
  }

  public function getQuotation() {
    return $this->quotation;
  }

  public function setAnswer($answers) {
    array_push($this->answers, $answers);
  }

  public function getAnswer() {
    return $this->answers;
  }

  public function setScore($score) {
    $this->score = $score;
  }

  public function getScore() {
    return $this->score;
  }

  /**
   * Queries of this class
   */

  /**
   * Check the answer of an DAD question
   */
  public function checkAnswer($form_state) {

    //get the item id
    $itemid = $this->getId();

    //get the ids from the textboxes
    $textboxIDS = explode("/", $form_state['values']['texboxenFIBDAD_' . $itemid]);
    $correctAnswers = explode("/", trim($form_state['values']['answerTextboxes_' . $itemid]));

    //get rid of the empty values in the array
    $textboxIDS = array_filter($textboxIDS);
    $correctAnswers = array_filter($correctAnswers);

    //initiatlize string for putting in the vaues of the textboxes
    $answerUser = NULL;
    $counter = 0;
    $score = 0;

    //initialize a color
    drupal_add_js(array('qtici' => array('qtici_textbox' => 0, 'qtici_color' => "black")), 'setting');

    //run through all the ids and get the value of the textbox
    foreach ($textboxIDS as $textboxID) {
      $answerUser = trim($_POST['textbox_' . $textboxID]);
      $correctNum = 0;

      if (isset($correctAnswers[$counter]) && strcmp($answerUser, trim($correctAnswers[$counter])) == 0) {
         $correctNum = 1;
      }

      if ($correctNum == 1) {
        drupal_add_js(array('qtici' => array('qtici_textbox' => $textboxID, 'qtici_color' => "green")), 'setting');
        $score++;
      }
      else {
        drupal_add_js(array('qtici' => array('qtici_textbox' => $textboxID, 'qtici_color' => "red")), 'setting');
      }

      $counter++;
    }

    // Call the JS here
    drupal_add_js(drupal_get_path('module', 'qtici') . '/js/textboxColorChanger.js');

    $returnArray["numberOfTextboxes"] = count($textboxIDS);
    $returnArray["score"] = $score;

    //look if the answer is correct
    if (count($textboxIDS) == $score) {
      $returnArray["trueFalse"] = TRUE;
    }
    else {
      $returnArray["trueFalse"] = FALSE;
    }

    return $returnArray;
  }

  //public function checkAnswerForTest($form, $form_state) {
    ////get the item id
    //$itemid = $this->getId();

    ////get the ids from the textboxes
    //$textboxIDS = explode("/", $form_state['values']['texboxenFIBDAD_' . $itemid]);
    //$correctAnswers = explode("/", trim($form_state['values']['answerTextboxes_' . $itemid]));

    ////get rid of the empty values in the array
    //$textboxIDS = array_filter($textboxIDS);
    //$correctAnswers = array_filter($correctAnswers);

    ////initiatlize string for putting in the vaues of the textboxes
    //$answerUser = NULL;
    //$counter = 0;
    //$score = 0;

    ////run through all the ids and get the value of the textbox
    //foreach ($textboxIDS as $textboxID) {
      //$answerUser .= trim($_POST['textbox_' . $textboxID]) . " ";
      //$answerArray = explode(' ', trim($_POST['textbox_' . $textboxID]));
      //$answerArray = array_filter($answerArray);
      //$correctNum = 0;

      //foreach ($answerArray as $word) {
        //if (isset($correctAnswers[$counter]) && strcmp($word, trim($correctAnswers[$counter])) == 0) {
          //$correctNum++;
        //}
        //$counter++;
      //}

      //if (count($answerArray) != 0 && count($answerArray) == $correctNum) {
        //$score++;
      //}
    //}


    //$returnArray["numberOfTextboxes"] = count($textboxIDS);
    //$returnArray["score"] = $score / count($textboxIDS);

    ////look if the answer is correct
    //if (strcmp(trim($form_state['values']['answerTextboxes_' . $itemid]), trim($answerUser)) == 0) {
      //$returnArray["trueFalse"] = TRUE;
    //}
    //else {
      //$returnArray["trueFalse"] = FALSE;
    //}

    //return $returnArray;
  //}

  /**
   * Make DAD exercise form
   */
  function makeExerciseForm(&$info, &$options = array(), &$possibilities = array()) {
    
    parent::makeExerciseForm($info, $options, $possibilities);
    //initialize variables
    $form = array();
    //run through all the possibilities
    foreach ($possibilities as $value) {
      //initialize the array of possibilitys and give the items a random order
      $DADarray[$value->id] = unserialize($value->answer);
    }
    shuffle($DADarray);
    $optionToolbar = '<div class="options">';
    foreach ($DADarray as $key => $value) {
      //options toolbar
      $optionToolbar .= '<div class="draggable">' . $value['value'] . '</div>';
    }

    $optionToolbar .= '</div><div class="clear"></div>';

    //$form['options_dd_' . $item->id] = array(
    //  '#markup' => $optionToolbar,
    //);

    $description = db_select('qtici_item', 'i')
            ->fields('i', array(
              'description',
            ))
            ->condition('id', $this->id, '=')
            ->execute()->fetchField();
    $description = unserialize($description);

    $form['options_dd_' . $this->id] = array(
      '#markup' => '<h2 style="margin-bottom: 15px">' . $this->title . '</h2>' . '<span style="padding-left: 5px">' . $description["value"] . $optionToolbar . '</span>',
    );

    // Get content (field should change name to avoid this)
    $content = db_select('qtici_item', 'i')
            ->fields('i', array(
              'content',
            ))
            ->condition('id', $this->id, '=')
            ->execute()->fetchField();
    $content = unserialize($content);
    //Decode and encode to get rid of weird symbols
    $content = htmlentities($content);
    $content = html_entity_decode($content, ENT_COMPAT, 'UTF-8');
    $info = _qtici_checkMedia($content, $this->id);
    _qtici_checkTextbox($content, TRUE);
    $form['question' . $this->id] = array(
      '#markup' => $content . "<br /><br />",
    );
    
    $form['#attached']['library'][] = drupal_add_library('qtici', 'DADQuestion');

    return $form;
  }
}
