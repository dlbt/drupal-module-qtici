<?php

class KPRIMQuestion extends Item {

  public $answers = array();
  public $randomOrder;
  public $score;

  public function __construct($values = array()) {
    parent::__construct($values, 'qtici_KPRIM');
  }

  public function setAnswer($answers) {
    array_push($this->answers, $answers);
  }

  public function getAnswer() {
    return $this->answers;
  }

  public function setRandomOrder($randomOrder) {
    if ($randomOrder == 'Yes') {
      $this->randomOrder = TRUE;
    }
    else {
      $this->randomOrder = FALSE;
    }
  }

  public function getRandomOrder() {
    return $this->randomOrder;
  }

  public function setScore($score) {
    $this->score = $score;
  }

  public function getScore() {
    return $this->score;
  }

  /**
   * Check the answer of an KRIM question
   */
  public function checkAnswer($form_state) {
    $itemid = $this->id;

    //get the checked answers
    $checkboxAnswers = array_filter($form_state['values']['item_' . $itemid]);
    $checkboxAnswersUserText = NULL;
    
    $returnArray = array();

    //loop through the given checked answers
    foreach ($checkboxAnswers as $checkboxAnswer) {

      //get the answer of the user out of the database
      $userAnswer = db_select('qtici_possibility', 'p')
          ->fields('p')
          ->condition('p.id', $checkboxAnswer)
          ->execute()
          ->fetchAll();

      //unserialize the answer
      $unserializedAnswer = unserialize($userAnswer[0]->answer);

      //set the test of the checked answer in a string
      $checkboxAnswersUserText .= $unserializedAnswer["value"] . " ";
    }

    //look if the answer is correct
    if (strcmp(trim($form_state['values']['answer_' . $itemid]), trim($checkboxAnswersUserText)) == 0) {
      $returnArray['trueFalse'] = TRUE;
    }
    else {
      $returnArray['trueFalse'] = FALSE;
    }
    
    $returnArray['score'] = 0;
    $returnArray['numberOfTextboxes'] = 0;
    
    return $returnArray;
  }

  //public function checkAnswerForTest($form, $form_state) {
    
    //$answers = $form_state["input"]['item_' . $this->id];
    //$correctanswer = $form_state["input"]['answer_' . $this->id];
    ////get the checked answers
    //$checkboxAnswers = array_filter($answers);
    //$checkboxAnswersUserText = NULL;

    ////loop through the given checked answers
    //foreach ($checkboxAnswers as $checkboxAnswer) {

      ////get the answer of the user out of the database
      //$userAnswer = db_select('qtici_possibility', 'p')
          //->fields('p')
          //->condition('p.id', $checkboxAnswer)
          //->execute()
          //->fetchAll();

      ////unserialize the answer
      //$unserializedAnswer = unserialize($userAnswer[0]->answer);

      ////set the test of the checked answer in a string
      //$checkboxAnswersUserText .= $unserializedAnswer["value"] . " ";
    //}

    //$returnArray = array();

    ////look if the answer is correct
    //if (strcmp(trim($correctanwser), trim($checkboxAnswersUserText)) == 0) {
      //$returnArray['trueFalse'] = TRUE;
    //}
    //else {
      //$returnArray['trueFalse'] = FALSE;
    //}
    
    //$returnArray['score'] = 0;
    //$returnArray['numberOfTextboxes'] = 0;
    
    //return $returnArray;
  //}

  public function parseXML($item) {
    $this->setRandomOrder((string) getDataIfExists($item, 'presentation', 'response_lid', 'render_choice', 'attributes()', 'shuffle'));
    $this->setScore((string) getDataIfExists($item, 'resprocessing', 'outcomes', 'decvar', 'attributes()', 'maxvalue'));
    // Set Type
    $this->setType('KPRIM');
    // Get answers
    foreach ($item->presentation->response_lid->render_choice->children() as $child) {
      $possibility = new Possibility();
      $answer['value'] = (string) getDataIfExists($child, 'response_label', 'material', 'mattext');
      $answer['format'] = (string) getDataIfExists($flow_label, 'response_label', 'material', 'mattext', 'texttype');
      $possibility->myConstruct(NULL, (string) getDataIfExists($child, 'response_label', 'attributes()', 'ident'), ElementTypes::RADIOBUTTON, NULL, serialize($answer), NULL, NULL, NULL);
      $this->setPossibility($possibility);
    }

    $results = $item->xpath('resprocessing/respcondition[conditionvar/and]');
    foreach ($results as $result) {
      foreach ($result->conditionvar->and->varequal as $arrayAnswer) {
        $id = substr($arrayAnswer, 0, strpos($arrayAnswer, ':'));
        $status = substr($arrayAnswer, strpos($arrayAnswer, ':') + 1);
        $answers[$id] = $status;
      }
    }
    $this->setAnswer($answers);

    parent::parseXML($item);
  }

}
