<?php

class RecorderQuestion extends Item {

  public $score;

  public function __construct($values = array()) {
    parent::__construct($values, 'qtici_REC');
  }
  
  public function setScore($score) {
    $this->score = $score;
  }

  public function getScore() {
    return $this->score;
  }

  /*
   * Make recorder question exercise form
   * The method expects the item for which the form should be made
   * and the options of the form
   */

  function makeExerciseForm(&$info, &$options = array(), &$possibilities = array()) {
    
    $form = parent::makeExerciseForm($info);

    $form['item_' . $this->id] = array(
      '#markup' => $this->question . $this->addRecorder(),
    );
    
    // Disable buttons
    $_SESSION['exercise']['show_answer_' . $this->id] = 5;
    $_SESSION['exercise']['check_answer_' . $this->id] = 5;

    return $form;
  }

  /**
   * Add recorder to a page
   */
  function addRecorder() {
    drupal_add_library('qtici', 'recorder');
    global $base_url;
    $html = '<div id="item_rec' . $this->id . '" style="width: 230px; height: 140px"></div>';
    $html .= '<input type="button" value="' . t('Record') . '" id="item_rec_but' . $this->id . '" class="form-submit" />';
    $html .= '<input type="button" value="' . t('Stop') . '" id="item_stop_but' . $this->id . '" class="form-submit" />';
    $html .= '<input type="button" value="' . t('Play') . '" id="item_play_but' . $this->id . '" class="form-submit" />';
    $html .= '<div id="time_' . $this->id . '"></div>';
    $swf = $base_url . '/' . drupal_get_path('module', 'qtici') . '/js/recorder/recorder.swf';
    drupal_add_js(array('recorder' => array('swf' => $swf)), 'setting');
    
    drupal_add_js(array('recorder' => array('id' => $this->id)), 'setting');
    drupal_add_library('qtici', 'recorder-conf');
    return $html;
  }
}
