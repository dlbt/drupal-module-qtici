<?php

class MarkerQuestion extends Item {

  public $answers = array();
  public $score;

  function myFullConstruct($item) {
    $this->type = $item->type;
    $this->title = $item->title;
    $this->objective = NULL;
    $this->feedback = NULL;
    $this->hint = NULL;
    $this->solutionFeedback = NULL;
    $this->max_attempts = $item->max_attempts;
    $this->possibilities = NULL;
    $this->question = $item->question;
    $this->id = $item->id;
    $this->quotation = $item->quotation;
    $this->answers = NULL;
    $this->score = $item->score;
  }

  public function __construct($values = array()) {
    parent::__construct($values, 'qtici_MRK');
  }

  public function getAnswers() {
    return $this->answers;
  }

  public function setAnswers($answers) {
    $this->answers = $answers;
  }

  public function getScore() {
    return $this->score;
  }

  public function setScore($score) {
    $this->score = $score;
  }

  /**
   * Functions of this class
   */

  /**
   * Check the answer of an MRK question
   */
  public function checkAnswer($form_state) {
    $itemid = $this->id;

    //initialize variables
    $studentAnswer = NULL;
    $databaseAnswer = NULL;
    $max_score = 0;
    $returnArray = array();
    $returnArray['score'] = 0;

    $returnArray["trueFalse"] = FALSE;
    //look if the hidden field with the selected text is filled in
    if (isset($form_state['values']['MRK_hidden_' . $itemid])) {

      //set the selectend value in a variable
      $studentAnswer = $this->MRK_userAnswers($form_state['values']['MRK_hidden_' . $itemid]);

      //get the array of the correct database answer
      $databaseAnswer = $this->MRK_validate();
      $databaseAnswer = array_filter($databaseAnswer);
      $max_score = count($databaseAnswer);

      // Compare arrays
      $diff = array_diff($studentAnswer, $databaseAnswer);
      $returnArray['score'] = $max_score - count($diff);
    }

    //look if the user answer is correct
    if (isset($diff) && empty($diff)) {
      $returnArray["trueFalse"] = TRUE;
    }

    $returnArray['numberOfTextboxes'] = 0;

    return $returnArray;
  }

  //public function checkAnswerForTest($form, $form_state) {

    //$hiddenvariables = $form_state["input"]['MRK_hidden_' . $this->id];
    ////initialize variables
    //$studentAnswer = NULL;
    //$databaseAnswer = NULL;
    //$max_score = 0;
    //$returnArray = array();
    //$returnArray['score'] = 0;

    //$returnArray["trueFalse"] = FALSE;
    ////look if the hidden field with the selected text is filled in
    //if (isset($hiddenvariables)) {

      ////set the selectend value in a variable
      //$studentAnswer = $this->MRK_userAnswers($hiddenvariables);
      ////get the array of the correct database answer
      //$databaseAnswer = $this->MRK_validate();
      //$databaseAnswer = array_filter($databaseAnswer);
      //$max_score = count($databaseAnswer);

      //// Compare arrays
      //$diff = array_diff($studentAnswer, $databaseAnswer);
      //$returnArray['score'] = $max_score - count($diff);
    //}

    ////look if the user answer is correct
    //if (isset($diff) && empty($diff)) {
      //$returnArray["trueFalse"] = TRUE;
    //}

    //$returnArray['numberOfTextboxes'] = 0;

    //return $returnArray;
  //}

  /**
   * Make MRK exercise form
   */
  function makeExerciseForm(&$info, &$options = array(), &$possibilities = array()) {
    
    $form = parent::makeExerciseForm($info, $options);

    //get the keys of the value for getting the first possibility
    $keys = array_keys($options);

    $form['MRK_hidden_' . $this->id] = array(
      '#type' => 'hidden',
    );

    $form['itemt_' . $this->id] = array(
      '#type' => 'item',
      '#required' => FALSE,
      '#title' => $this->question,
      '#description' => t(''),
      '#markup' =>
      "<style>
          .selectable_style .ui-selecting { background: #FECA40; }
          .selectable_style .ui-selected { background: #F39814; color: white; }
          .selectable_style { width: 80%; margin-left: 50px; }
          .selectable_style p {margin-left:20px; }
        </style>
        <div id=\"selectable_$this->id\">" . filter_xss($options[$keys[0]]) . "</div><span style=\"display:none\" id=\"item_$this->id\"></span><br/>",
    );
    
    $form['#attached']['library'][] = drupal_add_library('qtici', 'markerQuestion');

    return $form;
  }

  /**
   * Get Answers
   */

  function findAnswers() {
    $query = db_select('qtici_possibility', 'p')
      ->fields('p', array(
        'answer',
      ))
      ->condition('itemid', $this->id, '=')
      ->condition('is_correct', 1, '=')
      ->execute();

    // Markers must come from SCQ exercises, therefore only one result can popup
    $answer = $query->fetchField();
    $answer_u = unserialize($answer);

    return $answer_u;
  }

  /**
   * Return answer string for CheckAnswer
   */
  function MRK_validate() {
    $answer = $this->findAnswers();
    $result = _qtici_findAnswers($answer['value']);

    return $result;
  }

  /**
   * Get User answers
   */
  function MRK_userAnswers($numbers) {
    $answer = $this->findAnswers();
    $string = filter_xss($answer['value']);
    $string = strip_tags($string);
    $l_pos = explode('-', $numbers);

    $result = array();
    $word = '';
    $last_pos = FALSE;
    foreach ($l_pos as $position) {
      $letter = substr($string, $position - 1, 1);
      if (is_bool($last_pos) || $last_pos + 1 == $position) {
        $word .= $letter;
      }
      else {
        $result[] = $word;
        $word = $letter;
      }
      $last_pos = $position;
    }

    if ($last_pos == end($l_pos)) {
      $result[] = $word;
    }

    // Remove white spaces at the beginning or the end of the words
    $result_t = array_map('trim', $result);

    return $result_t;
  }
}

?>
