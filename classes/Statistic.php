<?php

class Statistic {

  private $id;
  private $testid;
  private $completed;
  private $time_spended;
  private $score;
  private $date_started;

  function __construct() {
  }

  function myConstruct($id, $testid, $completed, $time_spended, $score, $date_started) {
    $this->id = $id;
    $this->testid = $testid;
    $this->completed = $completed;
    $this->time_spended = $time_spended;
    $this->score = $score;
    $this->date_started = $date_started;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getTestid() {
    return $this->testid;
  }

  public function setTestid($testid) {
    $this->testid = $testid;
  }

  public function getCompleted() {
    return $this->completed;
  }

  public function setCompleted($completed) {
    $this->completed = $completed;
  }

  public function getTime_spended() {
    return $this->time_spended;
  }

  public function setTime_spended($time_spended) {
    $this->time_spended = $time_spended;
  }

  public function getScore() {
    return $this->score;
  }

  public function setScore($score) {
    $this->score = $score;
  }

  public function getDate_started() {
    return $this->date_started;
  }

  public function setDate_started($date_started) {
    $this->date_started = $date_started;
  }

  /**
   * Querries of this class
   */
  function insertTestStatistics($testid, $completed, $startTime, $score) {
    $date = time();

    $timeSpended = $date - $startTime;

    db_insert('qtici_test_statistics')
        ->fields(array(
          'testid' => $testid,
          'completed' => $completed,
          'time_spended' => $timeSpended,
          'score' => $score,
          'date_started' => $date,
        ))
        ->execute();
  }

  function getAllStatistics($level = '', $topic = '', $tag = NULL) {
    // Get all tests
    $tests = _qtici_get_exercisesLT($level, $topic, $tag);
    $statistic = array();
    // Get general fields
    // Total number of tests
    $numberTests = count($tests);
    // Total number of statistics inizialization
    $numberStats = NULL;
    // Tests totals
    $test_totals = array(
      // Total duration
      'duration' => NULL,
      // Total number of published tests
      'published' => NULL,
    );

    // Get all statistics
    foreach ($tests as $test) {
      if ($this->getAllStatisticsByTestID($test['id'])) {
        $statistic[] = $this->getAllStatisticsByTestID($test['id']);
        $numberStats++;
      }

      $testObj = qtici_test_entity_load($test['id']);

      foreach ($test_totals as $key => $value) {
        $test_totals[$key] += $testObj->$key;
      }
    }

    // Tests never done
    $neverDone = $numberTests - $numberStats;

    $testObj = new Test();

    // Total variables
    $totals = array(
      // Total score
      'score' => NULL,
      // Total maximum score
      'max_score' => NULL,
      // Total time spent
      'time_spended' => NULL,
      // Total number of test completed
      'completed' => NULL,
    );

    // Average variables
    $averages = array(
      // Average duration
      'duration' => NULL,
      // Average spent time
      'time_spended' => NULL,
      // Average score
      'score' => NULL,
    );
    // Date statistics started
    $date_started = REQUEST_TIME;
    foreach ($statistic as $stat) {
      if ($stat) {
        foreach ($totals as $key => $value) {
          if ($key !== 'max_score') {
            $totals[$key] += $stat[$key];
          }
          else {
            $totals[$key] = $value + $testObj->getMaximumScoreTest();
          }
        }

        foreach ($averages as $key => $value) {
          if ($key !== 'score') {
            $averages[$key] = $value + ($stat[$key])/$numberStats;
          }
          else {
            $averages[$key] = $value + ($stat[$key])/($numberStats*$testObj->getMaximumScoreTest());
          }
        }

        if ($stat['date_started'] < $date_started) {
          $date_started = $stat['date_started'];
        }
      }
    }
    $test_totals['durationTot'] = $test_totals['duration'];
    unset($test_totals['duration']);
    $totals['time_spended_tot'] = $totals['time_spended'];
    unset($totals['time_spended']);
    $totals['totalScore'] = $totals['score'];
    unset($totals['score']);
    $averages['durationAverage'] = $averages['duration'];
    unset($averages['duration']);
    $averages['scoreAverage'] = $averages['score'];
    unset($averages['score']);

    $statistics = array_merge($test_totals, $totals, $averages, array('numberTests' => $numberTests, 'neverDone' => $neverDone, 'numberStats' => $numberStats, 'date_started' => $date_started));

    return $statistics;
  }

  function getAllStatisticsByTestID($testid) {
    $query = db_select('qtici_test_statistics', 'ts');
    $query->fields('ts', array('id', 'testid'));
    $query->fields('t', array('id', 'olat_testid', 'title', 'description', 'duration', 'passing_score', 'published', 'answers_in', 'answers_out'));
    $query->join('qtici_test', 't', 'ts.testid = t.id');
    $query->addExpression('COUNT(ts.completed)', 'completed');
    $query->addExpression('AVG(ts.time_spended)', 'time_spended');
    $query->addExpression('AVG(ts.score)', 'score');
    $query->addExpression('MAX(ts.date_started)', 'date_started');
    $query->groupBy('t.id');
    $query->havingCondition('t.id', $testid, '=');
    $result = $query->execute()->fetchAssoc();

    return $result;
  }

  function checkIfStatisticsExist($testid) {
    $query = db_select('qtici_test_statistics', 'ts');
    $query->fields('ts', array('id', 'testid'));
    $query->fields('t', array('id'));
    $query->join('qtici_test', 't', 'ts.testid = t.id');
    $query->groupBy('t.id');
    $query->havingCondition('t.id', $testid, '=');
    $result = $query->execute()->fetchAssoc();

    if (empty($result)) {
      return false;
    }

    return true;
  }

}

?>
