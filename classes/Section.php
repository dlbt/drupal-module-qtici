<?php

class Section extends Entity {

  //public $ident;
  public $id;
  public $testid;
  public $title;
  public $objective;
  public $description;
  public $ordering;
  public $items = array();

  function __construct() {
    
  }

  function myConstruct($id) {
    $this->id = $id;
  }

  function myFullConstruct($section) {
    $this->id = $section->id;
    $this->testid = $section->testid;
    $this->title = $section->title;
    $this->description = $section->description;
    $this->ordering = $section->ordering;
    $this->items = NULL;
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getTestid() {
    return $this->testid;
  }

  public function setTestid($testid) {
    $this->testid = $testid;
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function getTitle() {
    return $this->title;
  }

  public function setObjective($objective) {
    $this->objective = $objective;
  }

  public function getObjective() {
    return $this->objective;
  }

  public function getDescription() {
    return $this->description;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function setOrdering($ordering) {
    $this->ordering = $ordering;
  }

  public function getOrdering() {
    return $this->ordering;
  }

  public function setItem($item) {
    array_push($this->items, $item);
  }

  public function getItems() {
    return $this->items;
  }

  /**
   * Queries of this class
   */
  public function deleteSection() {
    if ($this->id) {
      db_delete('qtici_section')
        ->condition('id', $this->id)
        ->execute();
    }
  }
  
  public function getItemsBySection() {

    $query = db_select('qtici_item', 'i');
    $query->fields('i');
    $query->join('qtici_section', 's', 's.id = i.sectionid');
    $query->condition('i.sectionid', $this->id, '=');
    $entitys = $query->execute()->fetchAll(PDO::FETCH_CLASS, 'Item');

    $result = array();
    foreach ($entitys as $entity) {
      $result[] = $entity;
    }

    return $result;
  }

  /**
   * Functions of this class
   */
  public function makeSectionForm($counter) {

    $form['section_info_' . $counter] = array(
      '#markup' => '<div class="expandable img_arrow" style="color:white; background-color:#757575; font-family:Helvetica, Arial, Sans-Serif; font-weight:bold; margin-bottom:15px">&nbsp;&nbsp;' . $this->title . '</div>
           <div class="divContent"><p>' . $this->description . '</p>',
    );

    return $form;
  }
}

?>
