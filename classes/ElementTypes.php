<?php
/**
 * Description of ElementTypes
 * This enumeration is especially used for FIB
 * --> determining whether is will be a text element or textbox
 *
 * @author Joey
 */
class ElementTypes {
  const TEXT = 'text';
  const TEXTBOX = 'textbox';
  const RADIOBUTTON = 'radiobutton';
  const CHECKBOX = 'checkbox';
  const IMAGE = 'image';
}

?>
