(function ($) {
  Drupal.behaviors.qticiMRK = {
    attach: function (context, settings) {
      $('[id^=selectable_]').each(function() {
        var currentId = $(this).attr('id');
        var toRemove = "selectable_";
        var id = currentId.replace(toRemove,'');
                
        var text = $("#"+currentId).text()
        text = text.trim()
        text = text.split("")
        text = text.join("</span><span class=\"ui-widget-content_"+id+"\">");
        $("#"+currentId).html("<span class=\"ui-widget-content_"+id+"\">" + text + "</span>");
        $('#selectable_'+id).addClass("selectable_style");
        
        /*$('#selectable_'+id+' span').each(function() {
          $(this).html($(this).text().replace(' ', '&nbsp;'));
        });*/
	
        $( "#selectable_"+id ).bind("mousedown", function(e) {
          e.metaKey = true;
        }).selectable({
          filter: "span.ui-widget-content_" +id,
          stop: function() {
            var c = 0;
            var woord ="" ;
            var result = $( "#selectable_"+id ).next('span').empty();
            $( ".ui-selected", this ).each(function() {
              var index = $( "#selectable_"+ id +" span" ).index( this );
              if(c == 0) {
                result.append( "" + ( index + 1 ) );
              } else {
                result.append( "-" + ( index + 1 ) ); 
              }
                                        
              letter = index + 1;
              if(c == 0){
                woord += ''+letter;
                c++;
              } else {
                woord += '-'+letter;
              }
            });
            $('input[name=MRK_hidden_'+ id +']').val(woord);
          }
        });
      });
    }
  }
})(jQuery);
