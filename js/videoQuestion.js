(function ($) {
  
  Drupal.qtici = Drupal.qtici || {};

  Drupal.behaviors.qticiVID = {
    attach: function(context, settings) {
      $('[name="grip_button"]').each(function() {
        $(this).click(function() {
          var id = $(this).attr('id');
          var itemid = id.replace('get_time_player_', '');
          Drupal.qtici.f_time_player(itemid);
        });
      });

      $('[name="qclear_button"]').each(function() {
        $(this).click(function() {
          var id = $(this).attr('class');
          var itemid = id.replace('qtici_clear_button_', '');
          Drupal.qtici.clearAnswer(itemid);
        });
      });
    }
  };
  
  Drupal.qtici.clearAnswer = function (itemid) {
    //get the number of possibilities for the question, then you also know how much textboxes there are for the possibiity
    number = document.getElementById("possibilitiesCount_" + itemid).value;
    //check if there is only 1 possibiity
    if (number === "0") {
      //clear the only input field
      document.getElementById("inputbox_time_player_" + itemid + "_id_0").value = "";
    } else {
      //loop through all input fields
      for (i = 0; i <= number; i++) {
        //clear the current input field
        document.getElementById("inputbox_time_player_" + itemid + "_id_" + i).value = "";
      }
    }
    //clear the hidden field that holds the times for checking them later
    document.getElementById("VID_hidden_" + itemid).value = "";
    //get the name of the flowplayer you want to work with
    var player = "player_" + itemid;
    //reset the player to the beginning
    $f(player).stop();
  };
  //template for displaying time in a nice format
  String.prototype.toHHMMSS = function() {
    sec_numb = parseInt(this, 10); // don't forget the second parm
    var hours = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);
    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    var time = minutes + ':' + seconds;
    return time;
  };
  
  Drupal.qtici.f_time_player = function (id) {
    //get the name of the flowplayer you want to work with
    var player = "player_" + id;
    //ask the state of the flowplayer playing, pauze,...
    var status = $f(player).getState();
    //if the flowplayer is playing get the time
    if (status == 3) {
      //get the number of possibilities for the question, then you also know how much textboxes there are for the possibiity
      number = document.getElementById("possibilitiesCount_" + id).value;
        
      //check if there is only 1 possibiity
      if (number === "0") {
        //when there is only one possibility check if it already has a time if not give it the current time
        if (document.getElementById("inputbox_time_player_" + id + "_id_0").value === "") {
          //set the current time in the textbox, the time is get in seconds and is rounded down to a full number, those seconds are converted with toHHMMSS to a nice time format
          document.getElementById("inputbox_time_player_" + id + "_id_0").value = Math.floor($f(player).getTime()).toString().toHHMMSS();
          //add the current time to de hidden field for checking it later
          document.getElementById("VID_hidden_" + id).value += $f(player).getTime() + "-";
        }
      } else {
        //if there are more than one input field go through them all
        for (i = 0; i <= number; i++) {
          //check if the current input field has an value
          if (document.getElementById("inputbox_time_player_" + id + "_id_" + i).value === "") {
            //set the current time in the textbox, the time is get in seconds and is rounded down to a full number, those seconds are converted with toHHMMSS to a nice time format
            document.getElementById("inputbox_time_player_" + id + "_id_" + i).value = Math.floor($f(player).getTime()).toString().toHHMMSS();
            //add the current time to de hidden field for checking it later
            document.getElementById("VID_hidden_" + id).value += $f(player).getTime() + "-";
            //if you gave this input field a time stop the for loop
            i = number + 1;
          }
        }
      }
      //pauze the player
      $f(player).pause();
      
    } else {
      //when the flowplayer isn't playing start him
      $f(player).play();
    }
    return false;
  };
})(jQuery);
