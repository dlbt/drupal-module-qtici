(function ($) {
  Drupal.behaviors.mediaplayer = {
    attach: function(context, settings) {
      // onload
      jQuery.each(settings.flowplayer, function(selector, config) {

        var type = 0;
        if (selector.substring(0, 5) == 'video') {
          type = 1;
        }
        var audioDiv = document.getElementsByClassName(selector);
        $(audioDiv).each(function(index) {
          if ($(this).className != 'mediaplayer-processed') {
            var audioTag = '<' + selector.substring(0, 5);
            if (type == 1) {
              audioTag = audioTag + ' width="480" height="270" controls="controls" preload="auto"';
            }
            audioTag = audioTag + ' id="' + selector + index + '">';
            var playlist;
            var sourceType;
            for (var i = 0; i < config['playlist'].length; i++) {
              playlist = config['playlist'][i].replace("%20", " ");
              sourceType = playlist.split('.').pop().toLowerCase();
              if (sourceType == 'mp3') {
                sourceType = 'mpeg';
              }
              audioTag = audioTag + '<source type="' + selector.substring(0, 5) + '/' + sourceType + '" src="' + playlist + '" />';
              if (type == 1) {
                flashdir = config['flashdir'];
                audioTag = audioTag + '<object type="application/x-shockwave-flash" data="' + flashdir + '"><<param name="movie" value="flashmediaelement.swf" /><param name="flashvars" value="controls=true&file=' + selector.substring(0, 5) + '" /></object>';
              }
            };
            audioTag = audioTag + '</' + selector.substring(0, 5) + '>';
            $(this).removeClass(selector).addClass(selector + index);
            $(this).html(audioTag);
            config_me = {
              // enables Flash and Silverlight to resize to content size
              enableAutosize: true,
              // the order of controls you want on the control bar (and other plugins below)
              features: ['playpause','progress','duration','volume'],
              // Hide controls when playing and mouse is not over the video
              alwaysShowControls: true,
            };
              
            if (type == 0) {
              // Add audio config
              config_me.audioWidth = 400;
              config_me.audioHeight = 30;
              config_me.loop = false;
            }
            else {
              // Add video config
              config_me.defaultVideoWidth = 480;
              config_me.defaultVideoHeight = 270;
              config_me.videoWidth = -1;
              config_me.videoHeight = -1;
            }

            $('#' + selector + index).addClass('mediaplayer-processed').mediaelementplayer(config_me);
          }
        });
      });
    }
  }
})(jQuery);
