(function ($) {
  Drupal.behaviors.qticiDAD = {
    attach: function (context, settings) {
      if ($("div.draggable").length > 0) {
        $("div.draggable").draggable({
          revert: function(droppableObj) {
            if(droppableObj === false) {
              $(this).data("ui-draggable").originalPosition = {
                top: 0,
                left: 0
              };
              var textbox = $(this).data("ui-droppable");
                $(textbox).width(74);
                $(textbox).val('');
                $(this).removeClass("dropped");
                        
              var selects = $('body').find('div.options');
              selects.each(function(index, el) {
                var children = el.children;
                for (var i = 0; i < children.length; i++) {
                  var child = children[i];
                  if ($(child).hasClass("dropped")) {
                    var textbox = $(child).data("ui-droppable");
                    $(child).position({
                      of: $(textbox),
                      my: 'left top',
                      at: 'left top'
                    });
                  }
                }
              });
                    
              return true;
            } else {
              var selects = $('body').find('div.options');
              selects.each(function(index, el) {
                var children = el.children;
                for (var i = 0; i < children.length; i++) {
                  var child = children[i];
                  if ($(child).hasClass("dropped")) {
                    var textbox = $(child).data("ui-droppable");
                    $(child).position({
                      of: $(textbox),
                      my: 'left top',
                      at: 'left top'
                    });
                  }
                }
              });
                        
              return false;
            }
          }
        });
      }
      //$("input.droppable").css("color","white");
      if ($("div.draggable").length > 0) {
        $("input.droppable").droppable({
          drop: function(event, ui) {
            ui.draggable.position({
              of: $(this),
              my: 'left top',
              at: 'left top'
            });
            $(this).draggable();
            $(this).draggable("widget").val(ui.draggable.text());
            ui.draggable.data("ui-droppable", this);
            ui.draggable.addClass("dropped");
            $(this).width(ui.draggable.width());
            $(this).height(ui.draggable.height());
          }
        });
      }
    }
  }
})(jQuery);
