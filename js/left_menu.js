(function($) {
  Drupal.behaviors.qtici = {
    attach: function(context, settings) {
      // If it is a test show only the corresponding menu
      if (typeof settings.qtici !== 'undefined' && typeof settings.qtici.test !== 'undefined') {
        var level = Drupal.settings.qtici.level;
        var levels = ["A", "B", "C"];
        for (i = 0; i < levels.length; i++) {
          if (levels[i] !== level) {
            $("#block-qtici-qtici_exercises_" + levels[i]).hide();
          }
        }
        var topic = Drupal.settings.qtici.topic;
        $(".topic_" + topic).children().each(function () {
          var name = $("a", this).attr("class");
          if (name === 'qtici-' + topic) {
            $("ul", this).show();
          }
        });

        if (typeof settings.qtici !== 'undefined' && typeof settings.qtici.testId !== 'undefined') {
          var testId = Drupal.settings.qtici.testId;
          $("ul.block_exercises li ul li").children().each(function () {
            var urlStr = $(this).attr("href");
            var name = urlStr.split('/').splice(-1,1);
            if (name == testId) {
              $(this).css("color", "#ce1433");
            }
          });
        }
      }
      else {

        if (typeof settings.qtici !== 'undefined' && typeof settings.qtici.topic !== 'undefined') {
          var topic = Drupal.settings.qtici.topic;
          $("ul.block_exercises").children().each(function () {
            var name = $("a", this).attr("class");
            if (name === 'qtici-' + topic) {
              $("ul", this).show();
            }
          });
        }
        if (typeof settings.qtici !== 'undefined' && typeof settings.qtici.level !== 'undefined') {
          var level = Drupal.settings.qtici.level;
          $("ul.block_exercises li ul li").children().each(function () {
            var name = $(this).attr("class");
            if (name !== 'qtici-' + level) {
              $(this).hide();
            }
          });
        }  
      }

      $("ul.block_exercises li").click( function() {
        var displayed = jQuery("ul", this).css("display");
        if (displayed === "none") {
          $("ul", this).slideDown(150);
        } else {
          $("ul", this).slideUp(150);
        };
      });
    }
  }
})(jQuery);
