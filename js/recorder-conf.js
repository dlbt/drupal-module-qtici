(function ($) {
  Drupal.behaviors.jRecorder = {
    attach: function(context, settings) {
      Recorder.initialize({
        swfSrc: settings.recorder.swf,                                  // URL to recorder.swf
        // optional:
        flashContainer: document.getElementById('item_rec' + settings.recorder.id), // (optional) element where recorder.swf will be placed (needs to be 230x140 px)
        onFlashSecurity: function(){                              // (optional) callback when the flash swf needs to be visible
                                                              // this allows you to hide/show the flashContainer element on demand.
        },
      });
      $('#item_rec_but' + settings.recorder.id).click(function() {
        Recorder.record({
          start: function(){
            //alert("recording starts now. press stop when youre done. and then play or upload if you want.");
          },
          progress: function(milliseconds){
            document.getElementById("time_" + settings.recorder.id).innerHTML = timecode(milliseconds);
          }
        });
      });

      $('#item_stop_but' + settings.recorder.id).click(function() {
        Recorder.stop();
      });

      $('#item_play_but' + settings.recorder.id).click(function() {
        Recorder.stop();
        Recorder.play({
          progress: function(milliseconds){
            document.getElementById("time_" + settings.recorder.id).innerHTML = timecode(milliseconds);
          }
        });
      });

      function timecode(ms) {
        var hms = {
          h: Math.floor(ms/(60*60*1000)),
          m: Math.floor((ms/60000) % 60),
          s: Math.floor((ms/1000) % 60)
        };
        var tc = []; // Timecode array to be joined with '.'

        if (hms.h > 0) {
          tc.push(hms.h);
        }

        tc.push((hms.m < 10 && hms.h > 0 ? "0" + hms.m : hms.m));
        tc.push((hms.s < 10 ? "0" + hms.s : hms.s));

        return tc.join(':');
      }
    }
  }
})(jQuery);
