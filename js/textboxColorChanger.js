(function($) {
  Drupal.behaviors.qtici = {
    attach: function (context, settings) {
      var counter = 0;
      $(settings.qtici.qtici_textbox, context).each(function() {
        $('#edit-textbox-' + this).css('outline-color', settings.qtici.qtici_color[counter]);
        $('#edit-textbox-' + this).css('outline-width', 'medium');
        $('#edit-textbox-' + this).css('outline-style', 'solid');
	//$('#edit-textbox-' + this).css('-moz-border-bottom-colors', settings.qtici.qtici_color[counter]);
        //$('#edit-textbox-' + this).css('border-bottom-width', 'medium');
	//$('#edit-textbox-' + this).css('border-top-width', 'thin');
	//$('#edit-textbox-' + this).css('border-right-width', 'thin');
	//$('#edit-textbox-' + this).css('border-left-width', 'thin');
        counter = counter + 1;
      });
    },
  };
})(jQuery);
