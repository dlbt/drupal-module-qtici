(function ($) {
  var start;
  $(document).ready(function() {
    var d = new Date();
    start = d.getTime();
    
    $("form#qtici-pagepreview").submit(function() {
      var d = new Date();
      end = d.getTime();
      timeSpent = end - start;
      $("input[name=time]").val(timeSpent);
      return true;
    });

    $("input[type=\"text\"]").keyup(function(){
      $(this).attr({width: "auto", size: $(this).val().length});
    });
  });
})(jQuery);
