<?php
class ChapterPage extends Chapter {

  private $htmlPage;

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($htmlPage) {
    $this->htmlPage = $htmlPage;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getChapterPage() {
    return $this->htmlPage;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setChapterPage($htmlPage) {
    $this->htmlPage = $htmlPage;
  }

}

?>
