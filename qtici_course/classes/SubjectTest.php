<?php
class SubjectTest extends Subject {

  private $test;

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($test) {
    $this->test= $test;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getSubjectTest() {
    return $this->test;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setSubjectTest($test) {
    $this->test = $test;
  }

}

?>
