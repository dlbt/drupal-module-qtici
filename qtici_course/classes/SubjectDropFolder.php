<?php
class SubjectDropFolder extends Subject {

  private $folders = array();


  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct() {
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getSubjectFolders() {
    return $this->folders;
  }

  //------------------------------------
  //
  // Beginning Set
  //ContentType --> hook_node_info (hook_node)
  //------------------------------------


  public function setSubjectFolders($folders) {
    array_push($this->folders, $folders);
  }

}

?>