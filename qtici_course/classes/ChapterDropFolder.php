<?php
class ChapterDropFolder extends Chapter {

  private $folders = array();


  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct() {
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getChapterFolders() {
    return $this->folders;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------


  public function setChapterFolders($folders) {
    array_push($this->folders, $folders);
  }

}

?>
