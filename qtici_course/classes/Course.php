<?php
include_once 'Chapter.php';
include_once 'ChapterPage.php';
include_once 'ChapterDropFolder.php';
include_once 'ChapterLearningObject.php';
include_once 'Subject.php';
include_once 'SubjectPage.php';
include_once 'SubjectDropFolder.php';
include_once 'SubjectLearningObject.php';
//include_once 'SubjectTest.php';
include 'Folder.php';
class course {

  private $id;
  private $shortTitle;
  private $longTitle;
  private $version;
  private $type;
  private $chapters = array();

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($id, $shortTitle, $longTitle, $version, $type) {
    $this->id = $id;
    $this->shortTitle = $shortTitle;
    $this->longTitle = $longTitle;
    $this->version = $version;
    $this->type = $type;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getId() {
    return $this->id;
  }

  public function getShortTitle() {
    return $this->shortTitle;
  }

  public function getLongTitle() {
    return $this->longTitle;
  }

  public function getVersion() {
    return $this->version;
  }

  public function getType() {
    return $this->type;
  }

  public function getChapters() {
    return $this->chapters;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setId($id) {
    $this->id = $id;
  }

  public function setShortTitle($shortTitle) {
    $this->shortTitle = $shortTitle;
  }

  public function setLongTitle($longTitle) {
    $this->longTitle = $longTitle;
  }

  public function setVersion($version) {
    $this->version = $version;
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function setChapter($chapters) {
    array_push($this->chapters, $chapters);
  }

}

?>
