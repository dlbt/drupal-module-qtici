<?php
class Chapter {

  private $id;
  private $shortTitle;
  private $type;
  private $visibilityBeginDate;
  private $visibilityEndDate;
  private $accessBeginDate;
  private $accessEndDate;
  private $subjects = array();

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($id = 0, $shortTitle = "", $type = "", $visibilityBeginDate ="", $visibilityEndDate = "", $accessBeginDate ="", $accessEndDate ="") {
    $this->id = $id;
    $this->shortTitle = $shortTitle;
    $this->type = $type;
    $this->visibilityBeginDate = $visibilityBeginDate;
    $this->visibilityEndDate = $visibilityEndDate;
    $this->accessBeginDate = $accessBeginDate;
    $this->accessEndDate = $accessEndDate;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getId() {
    return $this->id;
  }

  public function getShortTitle() {
    return $this->shortTitle;
  }

  public function getType() {
    return $this->type;
  }

  public function getVisibilityBeginDate() {
    return $this->visibilityBeginDate;
  }

  public function getVisibilityEndDate() {
    return $this->visibilityEndDate;
  }

  public function getAccessBeginDate() {
    return $this->accessBeginDate;
  }

  public function getAccessEndDate() {
    return $this->accessEndDate;
  }

  public function getSubjects() {
    return $this->subjects;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setId($id) {
    $this->id = $id;
  }

  public function setShortTitle($shortTitle) {
    $this->shortTitle = $shortTitle;
  }

  public function setType($type) {
    $this->type = $type;
  }

  function setVisibilityBeginDate($visibilityBeginDate) {
    $this->visibilityBeginDate = $visibilityBeginDate;
  }

  public function setVisibilityEndDate($visibilityEndDate) {
    $this->visibilityEndDate = $visibilityEndDate;
  }

  public function setAccessBeginDate($accessBeginDate) {
    $this->accessBeginDate = $accessBeginDate;
  }

  public function setAccessEndDate($accessEndDate) {
    $this->accessEndDate = $accessEndDate;
  }
  
  public function setSubject($subjects) {
    array_push($this->subjects, $subjects);
  }
  
}

?>

<?php
//
//include 'ChapterDropFolder.php';
//include 'ChapterPage.php';
//include 'Subject.php';
//class Chapter {
//
//  private $id;
//  private $shortTitle;
//  private $subjects = array();
//
//  //------------------------------------
//  //
//  // Beginning Constructor
//  //
//  //------------------------------------
//
//  public function __construct($id, $shortTitle) {
//    $this->id = $id;
//    $this->shortTitle = $shortTitle;
//  }
//
//  //------------------------------------
//  //
//  // Beginning Get
//  //
//  //------------------------------------
//
//  public function getId() {
//    return $this->id;
//  }
//
//  public function getShortTitle() {
//    return $this->shortTitle;
//  }
//
//  public function getSubjects() {
//    return $this->subjects;
//  }
//
//  //------------------------------------
//  //
//  // Beginning Set
//  //
//  //------------------------------------
//
//
//
//  public function setId($id) {
//    $this->id = $id;
//  }
//
//  public function setShortTitle($shortTitle) {
//    $this->shortTitle = $shortTitle;
//  }
//
//  public function setSubject($subjects) {
//    array_push($this->subjects, $subjects);
//  }
//  
//}
//
?>
