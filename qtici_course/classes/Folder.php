<?php
class Folder {

  private $fileName;
  private $fileLocation;
  private $fileSize;
  private $fileType;
  private $fileModified;

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($fileName ='', $fileLocation ='', $fileSize ='', $fileType ='', $fileModified ='') {
    $this->fileName = $fileName;
    $this->fileLocation = $fileLocation;
    $this->fileSize = $fileSize;
    $this->fileType = $fileType;
    $this->fileModified = $fileModified;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getFileName() {
    return $this->fileName;
  }

  public function getFileLocation() {
    return $this->fileLocation;
  }

  public function getFileSize() {
    return $this->fileSize;
  }

  public function getFileType() {
    return $this->fileType;
  }

  public function getFileModified() {
    return $this->fileModified;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setFileName($fileName) {
     $this->fileName = $fileName;
  }

  public function setFileLocation($fileLocation) {
     $this->fileLocation = $fileLocation;
  }

  public function setFileSize($fileSize) {
     $this->fileSize = $fileSize;
  }

  public function setFileType($fileType) {
     $this->fileType = $fileType;
  }

  public function setFileModified($fileModified) {
     $this->fileModified = $fileModified;
  }

}
?>
