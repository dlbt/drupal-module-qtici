<?php
class ChapterLearningObjectives extends Chapter {

  private $learningObjectives;

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($learningObjectives) {
    $this->learningObjectives = $learningObjectives;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getChapterLearningObjectives() {
    return $this->learningObjectives;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setChapterLearningObjectives($learningObjectives) {
    $this->learningObjectives = $learningObjectives;
  }

}

?>
