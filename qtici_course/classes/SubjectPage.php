<?php
class SubjectPage extends Subject {

  private $htmlPage;

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($htmlPage ="") {
    $this->htmlPage = $htmlPage;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getSubjectPage() {
    return $this->htmlPage;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setSubjectPage($htmlPage) {
    $this->htmlPage = $htmlPage;
  }

}

?>
