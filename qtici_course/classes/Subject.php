<?php
class Subject {

  private $id;
  private $shortTitle;
  private $type;
  private $subjects = array();

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------
  
  public function __construct($id ='', $shortTitle ='', $type ='') {
    $this->id = $id;
    $this->shortTitle = $shortTitle;
    $this->type = $type;
    }
  
 //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------
  
  public function getSubjectId() {
    return $this->id;
  }

  public function getSubjectShortTitle() {
    return $this->shortTitle;
  }

  public function getSubjectType() {
    return $this->type;
  }
    public function getSubjects() {
    return $this->subjects;
  }

    //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------
  
  

  public function setSubjectId($id) {
     $this->id = $id;
  }

  public function setSubjectShortTitle($shortTitle) {
     $this->shortTitle = $shortTitle;
  }

  public function setSubjectType($type) {
     $this->type = $type;
  }
    public function setSubject($subjects) {
    array_push($this->subjects, $subjects);
  }

}
?>
