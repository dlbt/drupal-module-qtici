<?php
class SubjectLearningObjectives extends Subject {

  private $learningObjectives;

  //------------------------------------
  //
  // Beginning Constructor
  //
  //------------------------------------

  public function __construct($learningObjectives) {
    $this->learningObjectives = $learningObjectives;
  }

  //------------------------------------
  //
  // Beginning Get
  //
  //------------------------------------

  public function getSubjectLearningObjectives() {
    return $this->learningObjectives;
  }

  //------------------------------------
  //
  // Beginning Set
  //
  //------------------------------------



  public function setSubjectLearningObjectives($learningObjectives) {
    $this->learningObjectives = $learningObjectives;
  }

}

?>
