<?php

/**
 * @file - All forms go in this file
 */

/**
 * This function will trigger curse_overview()
 */
function qtici_cursus_overview($form, &$form_state) {

  $form['module_cursus_overview'] = array(
    '#type' => 'markup',
    '#markup' => cursus_overview(),
  );

  return $form;
}

function show_course($from, &$form_state) {
  return array(
    'module_course' => array(
      '#type' => 'markup',
      '#markup' => course($from, $form_state),
    ),
  );
}

function qtici_conf($form, &$form_state) {
  if (user_access('teacher')) {
    return array(
      'qtici_conf' => array(
        '#type' => 'markup',
        '#markup' => conf($form, $form_state),
      ),
    );
  }
}
