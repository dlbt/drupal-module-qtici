<?php

/**
 * @file - Diverse functions
 */
 
function getSubjectsDb($subjects, $parent_chapter_id = NULL, $parent_subject_id = NULL) {

  if ($subjects == null)
    return;

  foreach ($subjects as $subjectObject) {
    //doe uw ding
    $subject_general_info_id = db_insert('qtici_course_general_info')
        ->fields(array(
          'general_id' => $subjectObject->getSubjectId(),
          'short_title' => $subjectObject->getSubjectShortTitle(),
          'type' => $subjectObject->getSubjectType(),
          'publish_status' => 1,
        ))
        ->execute();

    $subject_id = db_insert('qtici_course_subject')
        ->fields(array(
          'general_info_id' => $subject_general_info_id,
          'chapter_id' => $parent_chapter_id,
          'subject_id' => $parent_subject_id,
          'status' => 0,
        ))
        ->execute();


    $subject_function_id = db_insert('qtici_course_function')
        ->fields(array(
          'subject_id' => $subject_id,
        ))
        ->execute();

    switch ($subjectObject->getSubjectType()) {
      case "iqtest":
      case "iqself":
      case "iqsurv":
        break;
      case "en":
        $subject_learning_object_id = db_insert('qtici_course_learning_object')
            ->fields(array(
              'learning_object' => $subjectObject->getSubjectLearningObjectives(),
              'function_id' => $subject_function_id,
            ))
            ->execute();
        break;

      case "bc":

        foreach ($subjectObject->getSubjectFolders() as $FolderObject) {
          $subject_folder_id = db_insert('qtici_course_folder')
              ->fields(array(
                'name' => $FolderObject->getFileName(),
                'location' => $FolderObject->getFileLocation(),
                'type' => $FolderObject->getFileType(),
                'size' => $FolderObject->getFileSize(),
                'modified' => $FolderObject->getFileModified(),
              ))
              ->execute();

          $subject_drop_folder_id = db_insert('qtici_course_drop_folder')
              ->fields(array(
                'function_id' => $subject_function_id,
                'folder_id' => $subject_folder_id,
              ))
              ->execute();
        }


        break;

      case "sp":
      case "st":
        $subject_page_id = db_insert('qtici_course_page')
            ->fields(array(
              'location' => $subjectObject->getSubjectPage(),
              'function_id' => $subject_function_id,
            ))
            ->execute();
        break;
    }


    getSubjectsDb($subjectObject->getSubjects(), NULL, $subject_id);
  };
}

/**
 * This function will delete, publish and unpublish the courses,
 * if action is "delete_course" than you will delete the course
 * if action is checkbox_course than you will receive an array of id's and triggers the function 'checkbox_and_publish_course' which will publishes or unpublished the course
 * if action is (un)publish_course, the function 'checkbox_and_publish_course'  will be triggered, and it will publishes or unpublished the course
 * if action is checkbox than you will receive an array of id's and triggers the function 'checkbox_and_publish_config_course' which will publishes or unpublished chapters or subjects
 * if action is nor of the items above :), the function 'checkbox_and_publish_config_course'  will be triggered, and it will publishes or unpublished the chapter or subject
 */
function course_conf_page_callback() {

  //object from the class test for accessing his functions
  $testObj = new Test();

  if (user_access('teacher')) {
    $course_conf_info = $_POST["var"];
    $checkArray = $course_conf_info[0];
    $action = $course_conf_info[1];
    if ($action == 'delete_course') {

      $alsoDeleteTests = $course_conf_info[3];

      foreach ($checkArray as $data) {
        list($id, $i) = explode("-", $data);

        if ($alsoDeleteTests == 'true') {
          $results = find_iqTest($id);
          foreach ($results as $olatTestid) {
            $testObj->deleteTestByTestIDOROlatID(null, $olatTestid);
          }
        }

        $query = db_select('qtici_course', 'c');
        $query
            ->join('qtici_course_general_info', 'g', 'c.general_info_id = g.id'); //JOIN node with users
        $query
            ->fields('g', array('short_title', 'publish_status'))
            ->fields('c', array('id', 'status', 'general_info_id', 'filepath'))
            ->condition('c.id', $id, '=')
        ;
        $result = $query->execute();
        $path = drupal_get_path('module', 'qtici');
        foreach ($result as $item) {
          recursiveDelete($item->id);

          $course_deleted = db_delete('qtici_course')
              ->condition('id', $item->id)
              ->execute();
          $general_info_course_deleted = db_delete('qtici_course_general_info')
              ->condition('id', $item->general_info_id)
              ->execute();

          //delete files in /upload/files
          $filepath = $item->filepath;
          rrmdir('sites/default/files/qtici/' . $filepath);
        }
      }
    } else {
      if ($action == 'checkbox_course' || $action == '(un)publish_course') {
        if ($action == 'checkbox_course') {
          foreach ($checkArray as $array) {
            $data = $array;
            list($id, $i) = explode("-", $data);
            checkbox_and_publish_course($i, $id, 0);
          }
        } else if ($action == '(un)publish_course') {
          $id = $checkArray;
          $i = $course_conf_info[2];
          checkbox_and_publish_course($i, $id, 1);
        }
      } else {

        if ($action == 'checkbox') {
          $id_course = $course_conf_info[3];
          $Rev_checkArray = array_reverse($checkArray);
          foreach ($Rev_checkArray as $array) {
            $data = $array;
            list($id, $lvl, $i, $id_course, $parents) = explode("-", $data);

            checkbox_and_publish_config_course($i, $id, $lvl, $id_course, $parents);
          }
        } else {
          $id = $checkArray;
          $lvl = $action;
          $i = $course_conf_info[2];
          $id_course = $course_conf_info[3];
          $parents = $course_conf_info[4];

          checkbox_and_publish_config_course($i, $id, $lvl, $id_course, $parents);
        }
      }
    }
  }
  //exit;
}

/**
 * This function will publish or unpublish a course and is triggered by course_conf_page_callback
 */
function checkbox_and_publish_course($i = NULL, $id = NULL, $ch = NULL) {
  drupal_add_css(drupal_get_path('module', 'qtici') . '/css/images.css');

  if (user_access('teacher')) {
    $query = db_select('qtici_course', 'c');
    $query
        ->join('qtici_course_general_info', 'g', 'c.general_info_id = g.id'); //JOIN node with users
    $query
        ->fields('g', array('id', 'publish_status'))
        ->fields('c', array('status'))
        ->condition('c.id', $id, '=')
    ;
    $result = $query->execute();

    foreach ($result as $item) {
      if ($item->publish_status == 0) {

        $path = drupal_get_path('module', 'qtici');
        db_update('qtici_course_general_info')
            ->fields(array('publish_status' => 1))
            ->condition('id', $item->id, '=')
            ->execute();
        echo '<script type="text/javascript">';
        if ($item->status == 0) {
          echo '$("#img_publ_' . $i . '").attr(\'class\', \'img_published\');';
          echo '$("#img_publ_' . $i . '").attr(\'title\', \'' . t('Published') . '\');';
        } else {
          echo '$("#img_publ_' . $i . '").attr(\'class\', \'img_halfpublished\');';
          echo '$("#img_publ_' . $i . '").attr(\'title\', \'' . t('Published met unpublished items') . '\');';
        }
        echo '$("#selectCourseArray' . $i . '").each(function(){ this.checked = false; });';
        echo '$("#img_publ_' . $i . '").next().text("1");';
        echo '</script>';
      } else {
        db_update('qtici_course_general_info')
            ->fields(array('publish_status' => 0))
            ->condition('id', $item->id, '=')
            ->execute();
        $path = drupal_get_path('module', 'qtici');
        echo '<script type="text/javascript">';
        echo '$("#img_publ_' . $i . '").attr(\'class\', \'img_unpublished\');';
        echo '$("#img_publ_' . $i . '").attr(\'title\', \'' . t('Unpublished') . '\');';
        echo '$("#selectCourseArray' . $i . '").each(function(){ this.checked = false; });';
        echo '$("#img_publ_' . $i . '").next().text("0");';
        echo '</script>';
      }
    }
  }
}

/**
 * This function will publish or unpublish a chapter or subject and is triggered by course_conf_page_callback
 */
function checkbox_and_publish_config_course($i = NULL, $id = NULL, $lvl = NULL, $id_course = NULL, $parents = NULL) {
  drupal_add_css(drupal_get_path('module', 'qtici') . '/css/images.css');

  if (user_access('teacher')) {
    if ($lvl == 1) {
      $query = db_select('qtici_course_chapter', 'ch');
      $query
          ->join('qtici_course_general_info', 'g', 'ch.general_info_id = g.id'); //JOIN node with users
      $query
          ->fields('ch', array('status'))
          ->fields('g', array('id', 'publish_status'))
          ->condition('ch.id', $id, '=')
      ;
    } else {
      $query = db_select('qtici_course_subject', 'su');
      $query
          ->join('qtici_course_general_info', 'g', 'su.general_info_id = g.id'); //JOIN node with users
      $query
          ->fields('su', array('status'))
          ->fields('g', array('id', 'publish_status'))
          ->condition('su.id', $id, '=')
      ;
    }

    $result = $query->execute();
    $path = drupal_get_path('module', 'qtici');
    foreach ($result as $item) {


      $query = db_select('qtici_course', 'c');
      $query
          ->fields('c', array('status'))
          ->condition('c.id', $id_course, '=')
      ;
      $result_course = $query->execute();
      if ($item->publish_status == 0) {
        db_update('qtici_course_general_info')
            ->fields(array('publish_status' => 1))
            ->condition('id', $item->id, '=')
            ->execute();
        foreach ($result_course as $res_course) {
          $sum = $res_course->status - 1;

          db_update('qtici_course')
              ->fields(array('status' => $sum))
              ->condition('id', $id_course, '=')
              ->execute();
        }
        $new_status = NULL;
        $publish_status = 0;

        if ($lvl > 2) {
          for ($g = 0; $g < $lvl - 2; $g++) {
            $query = db_select('qtici_course_subject', 'su');
            $query
                ->fields('su', array('subject_id '))
                ->condition('su.id', $id, '=')
            ;
            $result = $query->execute();
            foreach ($result as $itemStat) {
              $id = $itemStat->subject_id;
            }
            $query = db_select('qtici_course_subject', 'su');
            $query
                ->join('qtici_course_general_info', 'g', 'su.general_info_id = g.id');
            $query
                ->fields('g', array('publish_status'))
                ->fields('su', array('status '))
                ->condition('su.id', $id, '=')
            ;
            $result = $query->execute();
            foreach ($result as $itemStat) {
              $new_status = $itemStat->status;
              $publish_status = $itemStat->publish_status;
            }
            $new_status = $new_status - 1;
            db_update('qtici_course_subject')
                ->fields(array('status' => $new_status))
                ->condition('id', $id, '=')
                ->execute();
            $parent = explode("-", $parents);
            $parent = array_reverse($parent);
            //  echo "<script>alert('$publish_status')</script>";
            if ($new_status != 0 && $publish_status != 0) {
              echo '<script type="text/javascript">';
              echo '$("#img_publ_' . $parent[$g] . '").attr(\'class\', \'img_halfpublished\');';
              echo '$("#img_publ_' . $parent[$g] . '").attr(\'title\', \'' . t('Published met unpublished items') . '\');';
              //  echo '$("#selectCourseArray' . $parent[$g] . '").each(function(){ this.checked = false; });';
              echo '</script>';
            } else if ($new_status == 0 && $publish_status != 0) {
              echo '<script type="text/javascript">';
              echo '$("#img_publ_' . $parent[$g] . '").attr(\'class\', \'img_published\');';
              echo '$("#img_publ_' . $parent[$g] . '").attr(\'title\', \'' . t('Published') . '\');';
              // echo '$("#selectCourseArray' . $parent[$g] . '").each(function(){ this.checked = false; });';
              echo '</script>';
            }
          }
        }
        if ($lvl > 1) {
          $query = db_select('qtici_course_subject', 'su');
          $query
              ->fields('su', array('chapter_id '))
              ->condition('su.id', $id, '=')
          ;
          $result = $query->execute();
          foreach ($result as $itemStat) {
            $id = $itemStat->chapter_id;
          }
          $query = db_select('qtici_course_chapter', 'ch');
          $query
              ->join('qtici_course_general_info', 'g', 'ch.general_info_id = g.id');
          $query
              ->fields('g', array('publish_status'))
              ->fields('ch', array('status'))
              ->condition('ch.id', $id, '=')
          ;
          $result = $query->execute();
          foreach ($result as $itemStat) {
            $new_status = $itemStat->status;
            $publish_status = $itemStat->publish_status;
          }
          $new_status = $new_status - 1;
          db_update('qtici_course_chapter')
              ->fields(array('status' => $new_status))
              ->condition('id', $id, '=')
              ->execute();
          $parent = explode("-", $parents);
          $parent = array_reverse($parent);

          if ($new_status != 0 && $publish_status != 0) {
            echo '<script type="text/javascript">';
            echo '$("#img_publ_' . $parent[count($parent) - 1] . '").attr(\'class\', \'img_halfpublished\');';
            echo '$("#img_publ_' . $parent[count($parent) - 1] . '").attr(\'title\', \'' . t('Published met unpublished items') . '\');';
            //   echo '$("#selectCourseArray' . $parent[count($parent)-2] . '").each(function(){ this.checked = false; });';
            echo '</script>';
          } else if ($new_status == 0 && $publish_status != 0) {
            echo '<script type="text/javascript">';
            echo '$("#img_publ_' . $parent[count($parent) - 1] . '").attr(\'class\', \'img_published\');';
            echo '$("#img_publ_' . $parent[count($parent) - 1] . '").attr(\'title\', \'' . t('Published') . '\');';
            //   echo '$("#selectCourseArray' . $i . '").each(function(){ this.checked = false; });';
            echo '</script>';
          }
        }
        if ($item->status == 0) {
          echo '<script type="text/javascript">';
          echo '$("#img_publ_' . $i . '").attr(\'class\', \'img_published\');';
          echo '$("#img_publ_' . $i . '").attr(\'title\', \'' . t('Published') . '\');';
          echo '$("#selectCourseArray' . $i . '").each(function(){ this.checked = false; });';
          echo '</script>';
        } else {
          echo '<script type="text/javascript">';
          echo '$("#img_publ_' . $i . '").attr(\'class\', \'img_halfpublished\');';
          echo '$("#img_publ_' . $i . '").attr(\'title\', \'' . t('Published met unpublished items') . '\');';
          echo '$("#selectCourseArray' . $i . '").each(function(){ this.checked = false; });';
          echo '</script>';
        }
      } else {
        db_update('qtici_course_general_info')
            ->fields(array('publish_status' => 0))
            ->condition('id', $item->id, '=')
            ->execute();
        foreach ($result_course as $res_course) {
          $sum = $res_course->status + 1;
          db_update('qtici_course')
              ->fields(array('status' => $sum))
              ->condition('id', $id_course, '=')
              ->execute();
        }
        $new_status = NULL;
        $publish_status = 0;
        if ($lvl > 2) {
          for ($g = 0; $g < $lvl - 2; $g++) {
            $query = db_select('qtici_course_subject', 'su');
            $query
                ->fields('su', array('subject_id '))
                ->condition('su.id', $id, '=')
            ;
            $result = $query->execute();
            foreach ($result as $itemStat) {
              $id = $itemStat->subject_id;
            }
            $query = db_select('qtici_course_subject', 'su');
            $query
                ->join('qtici_course_general_info', 'g', 'su.general_info_id = g.id');
            $query
                ->fields('g', array('publish_status'))
                ->fields('su', array('status '))
                ->condition('su.id', $id, '=')
            ;
            $result = $query->execute();
            foreach ($result as $itemStat) {
              $new_status = $itemStat->status;
              $publish_status = $itemStat->publish_status;
            }
            $new_status = $new_status + 1;
            db_update('qtici_course_subject')
                ->fields(array('status' => $new_status))
                ->condition('id', $id, '=')
                ->execute();
            $parent = explode("-", $parents);
            $parent = array_reverse($parent);
            //  echo "<script>alert('$publish_status')</script>";
            if ($new_status != 0 && $publish_status != 0) {
              echo '<script type="text/javascript">';
              echo '$("#img_publ_' . $parent[$g] . '").attr(\'class\', \'img_halfpublished\');';
              echo '$("#img_publ_' . $parent[$g] . '").attr(\'title\', \'' . t('Published met unpublished items') . '\');';
              echo '$("#img_publ_' . $parent[$g] . '").attr(\'alt\', \'' . t('published met unpublished items') . '\');';
              //  echo '$("#selectCourseArray' . $parent[$g] . '").each(function(){ this.checked = false; });';
              echo '</script>';
            }
          }
        }
        if ($lvl > 1) {
          $query = db_select('qtici_course_subject', 'su');
          $query
              ->fields('su', array('chapter_id '))
              ->condition('su.id', $id, '=')
          ;
          $result = $query->execute();
          foreach ($result as $itemStat) {
            $id = $itemStat->chapter_id;
          }
          $query = db_select('qtici_course_chapter', 'ch');
          $query
              ->join('qtici_course_general_info', 'g', 'ch.general_info_id = g.id');
          $query
              ->fields('g', array('publish_status'))
              ->fields('ch', array('status '))
              ->condition('ch.id', $id, '=')
          ;
          $result = $query->execute();
          foreach ($result as $itemStat) {
            $new_status = $itemStat->status;
            $publish_status = $itemStat->publish_status;
          }
          $new_status = $new_status + 1;
          db_update('qtici_course_chapter')
              ->fields(array('status' => $new_status))
              ->condition('id', $id, '=')
              ->execute();
          $parent = explode("-", $parents);
          $parent = array_reverse($parent);
          if ($new_status != 0 && $publish_status != 0) {
            echo '<script type="text/javascript">';
            echo '$("#img_publ_' . $parent[count($parent) - 1] . '").attr(\'class\', \'img_halfpublished\');';
            echo '$("#img_publ_' . $parent[count($parent) - 1] . '").attr(\'title\', \'' . t('Published met unpublished items') . '\');';
            //   echo '$("#selectCourseArray' . $parent[count($parent)-2] . '").each(function(){ this.checked = false; });';
            echo '</script>';
          }
        }


        echo '<script type="text/javascript">';
        echo '$("#img_publ_' . $i . '").attr(\'class\', \'img_unpublished\');';
        echo '$("#img_publ_' . $i . '").attr(\'title\', \'' . t('Unpublished') . '\');';
        echo '$("#selectCourseArray' . $i . '").each(function(){ this.checked = false; });';
//    $parents = explode("-", $parents);
//    foreach ($parents as $parent) {
//    echo '$("#img_publ_' . $parent . '").attr(\'src\', \'' . trim($path) . '/css/images/halfpublished.png\');';
//    echo '$("#img_publ_' . $parent . '").attr(\'title\', \'' . t('Published met unpublished items') . '\');';
//    echo '$("#img_publ_' . $parent . '").attr(\'alt\', \'' . t('published met unpublished items') . '\');';
//    };
        echo '</script>';
      }

      //$new_id = NULL;
    }
  }
}

/**
 *
 * this recursive function will delete all the chapters, ans subjects and triggers the function 'deleteFunction' for deleting the chapters or course function such as page, dropfolder and learning object
 */
function recursiveDelete($course_id = NULL, $chapter_id = NULL, $subject_id = NULL) {
  if (user_access('teacher')) {
    $type = 0;
    if ($course_id != NULL) {
      $type = 1;
      $query = db_select('qtici_course_chapter', 'ch');
      $query
          ->fields('ch', array('id', 'general_info_id'))
          ->condition('ch.course_id', $course_id, '=')
      ;
    } else if ($chapter_id != NULL) {
      $type = 2;
      $query = db_select('qtici_course_subject', 'su');
      $query
          ->fields('su', array('id', 'general_info_id'))
          ->condition('su.chapter_id', $chapter_id, '=');
    } else {
      $type = 3;
      $query = db_select('qtici_course_subject', 'su');
      $query
          ->fields('su', array('id', 'general_info_id'))
          ->condition('su.subject_id', $subject_id, '=');
    }
    $result = $query->execute();

    foreach ($result as $item) {

      $item_id = $item->id;
      $general_info_id = $item->general_info_id;
      if ($course_id != NULL) {
        $query = db_select('qtici_course_subject', 'su');
        $query
            ->fields('su', array('id', 'general_info_id'))
            ->condition('su.chapter_id', $item_id, '=');
      } else {
        $query = db_select('qtici_course_subject', 'su');
        $query
            ->fields('su', array('id', 'general_info_id'))
            ->condition('su.subject_id', $item_id, '=');
      }
      $amount = $query->execute();

      if ($amount->rowCount() > 0) {

        if ($type == 1) {
          deleteFunction($item_id);
          $chapter_deleted = db_delete('qtici_course_chapter')
              ->condition('id', $item_id)
              ->execute();
        } else if ($type > 1) {
          deleteFunction(NULL, $item_id);
          $subject_deleted = db_delete('qtici_course_subject')
              ->condition('id', $item_id)
              ->execute();
        }
        $general_info_deleted = db_delete('qtici_course_general_info')
            ->condition('id', $general_info_id)
            ->execute();

        if ($course_id != NULL) {
          $htmlArray = recursiveDelete(NULL, $item_id, NULL);
        } else {
          //echo "<script>alert(' gen id $general_info_id - id $item_id - type = $type - aantal rijen ".$amount->rowCount()."')</script>";
          $htmlArray = recursiveDelete(NULL, NULL, $item_id);
        }
      } else {
        if ($type == 1) {
          deleteFunction($item_id);
          $chapter_deleted = db_delete('qtici_course_chapter')
              ->condition('id', $item_id)
              ->execute();
        } else if ($type > 1) {
          deleteFunction(NULL, $item_id);
          $subject_deleted = db_delete('qtici_course_subject')
              ->condition('id', $item_id)
              ->execute();
        }
        $general_info_deleted = db_delete('qtici_course_general_info')
            ->condition('id', $general_info_id)
            ->execute();
      }
    }
  }
  return;
}

/**
 *
 * this function will delete the page, dropfolder or learning object
 */
function deleteFunction($chapter_id = NULL, $subject_id = NULL) {
  if (user_access('teacher')) {
    $function_id = NULL;
    if (isset($chapter_id)) {
      $query = db_select('qtici_course_function', 'fu');
      $query
          ->fields('fu', array('id'))
          ->condition('fu.chapter_id', $chapter_id, '=');
    } else {
      $query = db_select('qtici_course_function', 'fu');
      $query
          ->fields('fu', array('id'))
          ->condition('fu.subject_id', $subject_id, '=');
    }
    $result = $query->execute();
    foreach ($result as $item) {
      $function_id = $item->id;

      $function_deleted = db_delete('qtici_course_function')
          ->condition('id', $function_id)
          ->execute();
    }
    $query = db_select('qtici_course_learning_object', 'lo');
    $query
        ->fields('lo', array('id'))
        ->condition('lo.function_id', $function_id, '=');
    $result = $query->execute();
    if ($result->rowCount() > 0) {

      $learning_object_deleted = db_delete('qtici_course_learning_object')
          ->condition('function_id', $function_id)
          ->execute();
    } else {
      $query = db_select('qtici_course_page', 'p');
      $query
          ->fields('p', array('id'))
          ->condition('p.function_id', $function_id, '=');
      $result = $query->execute();
      if ($result->rowCount() > 0) {

        $page_deleted = db_delete('qtici_course_page')
            ->condition('function_id', $function_id)
            ->execute();
        /////////////////////////////////////////////////
      } else {
        $query = db_select('qtici_course_drop_folder', 'df');
        $query
            ->fields('df', array('id', 'folder_id'))
            ->condition('df.function_id', $function_id, '=');
        $result = $query->execute();
        if ($result->rowCount() > 0) {
          foreach ($result as $res) {

            $folder_deleted = db_delete('qtici_course_folder')
                ->condition('id', $res->folder_id)
                ->execute();
          }

          $dropfolder_deleted = db_delete('qtici_course_drop_folder')
              ->condition('function_id', $function_id)
              ->execute();
        }
      }
    }
  }
  return;
}

function find_iqTest($course_id) {
  $qti_string = "";
  $query = db_select('qtici_course', 'c');
  $query->join('qtici_course_general_info', 'g', 'c.general_info_id = g.id'); //JOIN node with users
  $query
      ->fields('g', array('general_id', 'type'))
      ->fields('c', array('id'))
      ->condition('c.id', $course_id, '=');
  $result = $query->execute();
  foreach ($result as $item) {
    if ($item->type == 'iqtest' || $item->type == 'iqself' || $item->type == 'iqsurv') {
      $qti_string .= $item->general_id;
    }
    $query = db_select('qtici_course_chapter', 'ch');
    $query->join('qtici_course_general_info', 'g', 'ch.general_info_id = g.id'); //JOIN node with users
    $query
        ->fields('g', array('general_id', 'type'))
        ->fields('ch', array('id'))
        ->condition('ch.course_id', $item->id, '=');
    $result_chapter = $query->execute();
    foreach ($result_chapter as $item_chapter) {
      if ($item_chapter->type == 'iqtest' || $item_chapter->type == 'iqself' || $item_chapter->type == 'iqsurv') {
        $qti_string .= '+' . $item_chapter->general_id;
      }

      $query = db_select('qtici_course_subject', 's');
      $query->join('qtici_course_general_info', 'g', 's.general_info_id = g.id'); //JOIN node with users
      $query
          ->fields('g', array('general_id', 'type'))
          ->fields('s', array('id'))
          ->condition('s.chapter_id', $item_chapter->id, '=');
      $result_subject = $query->execute();
      foreach ($result_subject as $item_subject) {


        $recursive_qti = recursive_qti($item_subject->id, $item_subject->general_id, null, $item_subject->type);

        $qti_string .= "$recursive_qti";
      }
    }
  }
  if (substr($qti_string, 0, 1) == '+') {
    $qti_string = substr($qti_string, 1);
  }
  return explode("+", $qti_string);
}

function recursive_qti($id, $general_id, $qti_string = NULL, $type = NULL) {
  $qti_string = $qti_string;
  $type = $type;
  if ($type == 'iqtest' || $type == 'iqself' || $type == 'iqsurv') {
    $qti_string .= '+' . $general_id;
  }
  $query = db_select('qtici_course_subject', 'su');
  $query->join('qtici_course_general_info', 'g', 'su.general_info_id = g.id');
  $query
      ->fields('g', array('general_id', 'type'))
      ->fields('su', array('id'))
      ->condition('su.subject_id', $id, '=');
  $result_subject = $query->execute();

  if ($result_subject->rowCount() > 0) {
    foreach ($result_subject as $item) {

      $qti_string .= '' . recursive_qti($item->id, $item->general_id, $qti_string, $item->type);

      return "$qti_string";
    }
  } else {
    return "$qti_string";
  }
}
