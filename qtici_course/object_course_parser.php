<?php

/**
 *
 * This script converts courses from olat into an object.
 * 
 * Dieter Verbeemen
 */
function parseCourseObject($filename) {
  // $course = 85235053647606;
  // $path = "/opt/olat/olatdata/bcroot/course";
  $pathCourse = substr("$filename", 0, -16);

  include_once 'classes/Course.php';
  $doc = new DOMDocument();
  if (file_exists($filename)) {
    $doc->loadXML(file_get_contents($filename));
    $xpaths = simplexml_load_file($filename, 'SimpleXMLElement', LIBXML_NOCDATA);
  }
  else {
    exit('Failed to open ' . $filename);
  }

// Course

  $result = $xpaths->xpath('/org.olat.course.Structure');
  $item = $result[0];
  $courseObject = new course(
          isset($item->rootNode->ident) ? (string) $item->rootNode->ident : null,
          isset($item->rootNode->shortTitle) ? (string) $item->rootNode->shortTitle : null,
          isset($item->rootNode->longTitle) ? (string) $item->rootNode->longTitle : null,
          isset($item->version) ? (string) $item->version : null,
          isset($item->rootNode->type) ? (string) $item->rootNode->type : null);

// Chapters
 //// $chapters = $xpaths->xpath("/org.olat.course.Structure/rootNode/children/*[type = 'st' or type = 'sp' or type = 'bc' or type = 'iqtest']");
$chapters = $xpaths->xpath("/org.olat.course.Structure/rootNode/children/*[type = 'st' or type = 'sp' or type = 'bc' or type = 'en' or type = 'iqtest' or type = 'iqself' or type = 'iqsurv']");
  foreach ($chapters as $item) {
    // if noPage = 0 at the end, than the type would be sp or st, and have no page in it, 
    $noPage = 0;
    switch ($item->type) {
      case "iqtest":
      case "iqself":
      case "iqsurv":
        $noPage ++;
        $chapterObject = new Chapter;
        break;

      case "en":
        $noPage ++;
        $chapterLearningObject = $xpaths->xpath("//*[ident = '$item->ident']/learningObjectives");
        foreach ($chapterLearningObject as $chapterLearningObjectItems) {
          $chapterLearningObjectItem = (string)$chapterLearningObjectItems;
        }

        $chapterObject = new ChapterLearningObjectives((string)$chapterLearningObjectItem);
        break;

      case "bc":
        // if noPage = 0 at the end, than the type would be sp or st, and have no page in it, 
        $noPage ++;
        $chapterObject = new ChapterDropFolder();
        $course_map = getDirectoryList("$pathCourse" . "export/$item->ident");

        for ($i = 0; count($course_map) > $i; $i++) {
          $location = "$pathCourse" . "export/$item->ident/$course_map[$i]";
          $FolderObject = new Folder(
                  (string) $course_map[$i],
                  (string) $location,
                  (string) filesize($location), 
                  (string) filetype($location),
                  (string) date("F d Y H:i:s.", filemtime($location)));
          $chapterObject->setChapterFolders($FolderObject);
        }


        break;

      case "sp":
        $chapterPagePath = $xpaths->xpath("//*[ident = '$item->ident']/moduleConfiguration/config//string[starts-with(.,'/')]");

        foreach ($chapterPagePath as $chapterPage) {
          $chapterPageItem = $chapterPage;
          $noPage ++;
        }
        if (!empty($chapterPageItem)) {
          $chapterObject = new ChapterPage((string) $chapterPageItem);
        }
        break;
      case "st":
        $chapterPagePath = $xpaths->xpath("//*[ident = '$item->ident']/moduleConfiguration/config//string[starts-with(.,'/')]");

        foreach ($chapterPagePath as $chapterPage) {
          $chapterPageItem = $chapterPage;
          $noPage ++;
        }
        if (!empty($chapterPageItem)) {
          $chapterObject = new ChapterPage((string) $chapterPageItem);
        } 
        else {
          $emptyHTML = "";
          $chapterObject = new ChapterPage($emptyHTML);
          $noPage ++;
        }
        break;
    }

    if($noPage != 0){
    $chapterObject->setId(isset($item->ident) ? (string) $item->ident : null);
    $chapterObject->setShortTitle(isset($item->shortTitle) ? (string) $item->shortTitle : null);
    $chapterObject->setType(isset($item->type) ? (string) $item->type : null);
    $chapterObject->setVisibilityBeginDate(isset($item->preConditionVisibility->easyModeBeginDate) ? (string) $item->preConditionVisibility->easyModeBeginDate : null);
    $chapterObject->setVisibilityEndDate(isset($item->preConditionVisibility->easyModeEndDate) ? (string) $item->preConditionVisibility->easyModeEndDate : null);
    $chapterObject->setAccessBeginDate(isset($item->preConditionAccess->easyModeBeginDate) ? (string) $item->preConditionAccess->easyModeBeginDate : null);
    $chapterObject->setAccessEndDate(isset($item->preConditionAccess->easyModeEndDate) ? (string) $item->preConditionAccess->easyModeEndDate : null);

    //Subjects
    getSubjects($chapterObject, $item->ident, $xpaths, $pathCourse);
    $courseObject->setChapter($chapterObject);
  }}
  return $courseObject;
}

function getSubjects(&$object, $id, $xpaths, $pathCourse) {
  // if noPag = 0 at the end, than the type would be sp or st, and have no page in it, 
  $noPag = 0;
  $subjects = $xpaths->xpath("/org.olat.course.Structure//*[ident='" . $id . "']/children/*[type = 'st' or type = 'sp' or type = 'bc' or type = 'en' or type = 'iqtest' or type = 'iqself' or type = 'iqsurv']");
  if ($subjects != null) {
    foreach ($subjects as $item) {

      switch ($item->type) {
        case "iqtest":
        case "iqself":
        case "iqsurv":
          $noPag ++;
          $subjectObject = new Subject;
          break;

        case "en":
          $noPag ++;
          $subjectLearningObject = $xpaths->xpath("//*[ident = '$item->ident']/learningObjectives");

          foreach ($subjectLearningObject as $subjecLearningObjects) {
            $learningObjectives = $subjecLearningObjects;
          }


          $subjectObject = new SubjectLearningObjectives((string) $learningObjectives);
          break;

        case "bc":
          $noPag ++;
          $subjectObject = new SubjectDropFolder();
          $course_map = getDirectoryList("$pathCourse" . "export/$item->ident");
          for ($i = 0; count($course_map) > $i; $i++) {
            $location = "$pathCourse" . "export/$item->ident/$course_map[$i]";

            $FolderObject = new Folder(
                    (string) $course_map[$i],
                    (string) $location,
                    (string) filesize($location),
                    (string) filetype($location),
                    (string) date("F d Y H:i:s.", filemtime($location)));
            $subjectObject->setSubjectFolders($FolderObject);
          }


          break;

        case "sp":
        case "st":
          $subjectPagePath = $xpaths->xpath("//*[ident = '$item->ident']/moduleConfiguration/config//string[starts-with(.,'/')]");

          foreach ($subjectPagePath as $subjecPage) {
            $htmlPage = $subjecPage;
            $noPag ++;
          }
          if (!empty($htmlPage)) {
            $subjectObject = new SubjectPage((string) $htmlPage);
          }
          break;
      }
      if($noPag !=0){
      $subjectObject->setSubjectId(isset($item->ident) ? (string) $item->ident : null);
      $subjectObject->setSubjectShortTitle(isset($item->shortTitle) ? (string) $item->shortTitle : null);
      $subjectObject->setSubjectType(isset($item->type) ? (string) $item->type : null);

//      // *********************** dit is de recursie !!!! ******************************
      getSubjects($subjectObject, $item->ident, $xpaths, $pathCourse);
      $object->setSubject($subjectObject);
    }}
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Extra functions
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function getDirectoryList($directory) {
// create an array to hold directory list
  $results = array();
  if (file_exists($directory)) {


// create a handler for the directory
    $handler = opendir($directory);

// open directory and walk through the filenames

    while ($file = readdir($handler)) {
// if file isn't this directory or its parent, add it to the results
      if ($file != "." && $file != "..") {
        $results[] = $file;
      }
    }
// tidy up: close the handler
    closedir($handler);
  }
// done!
  return $results;
}

function format_bytes($a_bytes) {
  if ($a_bytes < 1024) {
    return $a_bytes . ' B';
  }
  elseif ($a_bytes < 1048576) {
    return round($a_bytes / 1024, 2) . ' KB';
  }
  elseif ($a_bytes < 1073741824) {
    return round($a_bytes / 1048576, 2) . ' MB';
  }
  elseif ($a_bytes < 1099511627776) {
    return round($a_bytes / 1073741824, 2) . ' GB';
  }
  elseif ($a_bytes < 1125899906842624) {
    return round($a_bytes / 1099511627776, 2) . ' TB';
  }
  elseif ($a_bytes < 1152921504606846976) {
    return round($a_bytes / 1125899906842624, 2) . ' PB';
  }
  elseif ($a_bytes < 1180591620717411303424) {
    return round($a_bytes / 1152921504606846976, 2) . ' EB';
  }
  elseif ($a_bytes < 1208925819614629174706176) {
    return round($a_bytes / 1180591620717411303424, 2) . ' ZB';
  }
  else {
    return round($a_bytes / 1208925819614629174706176, 2) . ' YB';
  }
}

?>
