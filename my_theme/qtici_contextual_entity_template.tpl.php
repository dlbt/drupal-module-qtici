<div class="contextual-links-wrapper">
    <ul class="contextual-links">
        <?php foreach($links as $link): ?>
        <li>
            <?php echo l($link[1], $link[0]) ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>