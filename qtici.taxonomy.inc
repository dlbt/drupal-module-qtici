<?php
/**
 * @file - All functions to get taxonomy working with this module come here
 */

/**
 * Returns a list of entities currently asociated to a tag
 */
function _qtici_taxonomy_listByTag($tid = NULL) {
  
  $list = '';
  
  if (empty($tid)) {
    $list = _qtici_get_tag_cloud(); 
  }
  else {
    $query = db_select('taxonomy_entity_index', 'i');
    $query->fields('i', array(
      'entity_id',
    ));
    $query->condition('i.bundle', 'qtici_test', '=');
    $query->condition('i.tid', $tid, '=');
    $ids = $query->execute()->fetchCol();

    $tests = qtici_test_entity_load_multiple($ids);
    $items = array();

    foreach ($tests as $test) {
      $items[] = l($test->title, 'qtici/test/' . $test->id);
    }
    $term = taxonomy_term_load($tid);
    
    $list .= '<h2>' . $term->name . '</h2>';
    $list .= theme('item_list', array('items' => $items, 'type' => 'ul', 'attributes' => array('class' => 'taxonomy_list')));

    $list .= '<p>' . l('All categories', 'qtici/taxonomy') . '</p>';
  }

  return $list;
}

/**
 * Returns a plain list with the tags associated with an entity
 */
function _qtici_get_tag_list($entity) {
  
  $rray = array();
  $tags = _qtici_tag_query($entity->id);
  
  foreach ($tags as $tag) {
    $rray[] = l($tag->name, 'qtici/taxonomy/' . $tag->tid);
  }

  if (empty($rray)) {
    $text = t('There are no categories defined for this test');
  }
  else {
    $text = implode(' ', $rray);
  }
 
  $list = '<h3>' . t('Categories:') . ' ' . $text . '</h3>';
 
  return $list;
}

/**
 * Tag query: returns an object with the tags of an entity, or all available tags
 */
function _qtici_tag_query($id = NULL) {

  $query = db_select('taxonomy_term_data', 'd');
  $query->leftJoin('taxonomy_entity_index', 'i', 'i.tid = d.tid');
  $query->fields('d', array(
    'tid',
    'name',
  ));
  $query->condition('i.bundle', 'qtici_test', '=');
  if (isset($id)) {
    $query->condition('i.entity_id', $id, '=');
  }
  $query->addExpression('COUNT(i.tid)', 'count_tid');
  $query->orderBy('count_tid', 'DESC');
  $query->groupBy('tid');
  $tags = $query->execute();

  return $tags;
}

/**
 * Tag Cloud: returns a paragraph of tags styled by weigth
 */
function _qtici_get_tag_cloud() {
  $tags = _qtici_tag_query();

  $rray = array();
 
  $max = NULL;
  $min = NULL;
  $i = 1;
  $terms = array();
  // First loop to calculate max and min number of reps
  foreach ($tags as $tag) {
    if ($i == 1) {
      $max = $tag->count_tid;
    }
    $min = $tag->count_tid;  
    $i++;
    $terms[] = $tag;
  }
 
  foreach ($terms as $tag) {
    $size = 10 + (($max - ($max - ($tag->count_tid - $min)))*62/ ($max - $min));
    $rray[] = l($tag->name, 'qtici/taxonomy/' . $tag->tid, array('attributes' => array('style' => 'font-size:' . $size . 'px;',)));
  }
  shuffle($rray);
  $list = '<p>' . implode(' ', $rray) . '</p>';

  if (user_access('teacher')) {
    $list .= '<p>' . t('Manage categories from !here', array('!here' => l(t('here'), 'admin/structure/taxonomy/tags'))) . '</p>';
  }

  return $list;
}
